//******************************* Init Page level modules **************************//
$( document ).ready(function() {
	$( '.myclass' ).each(function () {
		if ($(this).text() == 'Deutsch') {
			$('.login-header').attr('style','width:80px !important');
			$('.langheader').attr('style','width:120px !important');
			
	    }			
	});
});

function runSafely(func, $debug) {
	try {
		func();
	} catch (e) {
		$debug = $debug || (func.name + '()');
		debug('error on ' + $debug);
	}
}

//debug js in console
function debug(msg, jq) {
	
	try {
		if (!console || !console.log) return;
		console.log(msg);
		if (jq != null) {
			console.log(jq);
		}
	} catch (e) {
		//only works with console enabled
	}
}

function initLogoutForm(){
	$("#logoutLink").on("click", function(e){
		   e.preventDefault();
		   ////console.log("logout clicked");
		   $("#logoutForm").submit();
	});	   
}		
//***********************************************************************************/

function searchDropDown(){
	/**
	* the element
	*/
	var $ui = $('#ui_element');
	
	/**
	* on focus and on click display the dropdown, 
	* and change the arrow image
	*/
	$('#dropdownSearch').bind('click',function(){
		$ui.find('.sb_down')
		   .addClass('sb_up')
		   .removeClass('sb_down')
		   .andSelf()
		   .find('.sb_dropdown')
		   .show();
		$ui.find('.sb_down')
		   .addClass('sb_up')
		   .removeClass('sb_down')
		   .andSelf()
		   .find('#search-form')
		   .show();   
	});
	
	/**
	* on mouse leave hide the dropdown, 
	* and change the arrow image
	*/
	$ui.bind('mouseleave',function(){
		
		$ui.find('.sb_up')
		   .addClass('sb_down')
		   .removeClass('sb_up')
		   .andSelf()
		   .find('#search-form')
		    
		$ui.find('.sb_up')
		   .addClass('sb_down')
		   .removeClass('sb_up')
		   .andSelf()
		   .find('.sb_dropdown')
		  
	});
	
	$(".btn-group").click( function(){
	    $('#ui_element').hide();
	});
	
	$(".message-header").click( function(){
	    $('#ui_element').hide();
	});

	$(document).on('click', function (e) {
	    if ($(e.target).closest("#ui_element").length === 0) {
	        $("#ui_element").hide();
	    }
	});
	
	$(dropdownSearch).click( function(){
	    $('#ui_element').show();  
	});
	
	/**
	* selecting all checkboxes
	*/
	$ui.find('.sb_dropdown').find('label[for="all"]').prev().bind('click',function(){
		$(this).parent().siblings().find(':checkbox').attr('checked',this.checked).attr('disabled',this.checked);
	});
}

$( "#searchText" ).keypress(function( event ) {
	
	if ( event.which == 13 ) {
		searchglobal();
		event.preventDefault();
	}
	
	
});
$(window).scroll(function(){
    if ($(this).scrollTop() > 135) {
        $('.scroll-to-top').show();
    } else {
        $('.scroll-to-top').hide();
    }
});

//******************************* Init Chefya Global Search **************************//
function globalSearch(){
	// Reset page posts if they were present
	$("#loadingPosts").show();
	if($(".dishes").length>0){
		$(".dishes").remove();
		$("#dishes-button").remove();
		$("#sorry-dishes").remove();
	}
	if($(".recipes").length>0){
		$(".recipes").remove();
		$("#recipes-button").remove();
		$("#sorry-recipes").remove();
	}
	if($(".productposts").length>0){
		$(".productposts").remove();
		$("#productposts-button").remove();
		$("#sorry-productposts").remove();
	}
	if($(".pictures").length>0){
		$(".pictures").remove();
		$("#pictures-button").remove();
		$("#sorry-pictures").remove();
	}
	if($(".videos").length>0){
		$(".videos").remove();
		$("#videos-button").remove();
		$("#sorry-videos").remove();
	}
	if($(".persons").length>0){
		$(".persons").remove();
		$("#persons-button").remove();
		$("#sorry-persons").remove();
	}
	$('.toolbar').hide();
	// Fetch search results based on global search params if present otherwise params ll be NULL
	$.ajax({
		type : "GET",
		contentType: "application/json",
		url : contextPath+"globalSearchForPostsByType",
		data: searchQueries,
		
		success : function(hashMap) {
			$("#loadingPosts").hide();
			$('.toolbar').show();
			$("#loadingPosts").hide();
			if(hashMap.dish!=undefined && hashMap.dish!=null){
				
				//console.log("sucess got dish length:"+hashMap.dish.length);
				
				populateSearchDiv("dishes", $.i18n.prop('welcome.dishes'), true);
				for(var i=0;i<hashMap.dish.length;i++){
					populateSearchPostsonGlobalSearchPage( hashMap.dish[i], "dishes");
					//bindDishExploreEvents();
				}
				if(hashMap.dish.length < 4){
					$("#dishes-button").css("visibility", 'hidden');
				}
			}
			if(hashMap.recipe!=undefined && hashMap.recipe!=null){
				
				//console.log("sucess got recipe length:"+hashMap.recipe.length);
				
				populateSearchDiv("recipes", $.i18n.prop('welcome.recipes'), true);
				for(var i=0;i<hashMap.recipe.length;i++){
					populateSearchPostsonGlobalSearchPage( hashMap.recipe[i], "recipes");
					//bindDishExploreEvents();
				}
				if(hashMap.recipe.length < 4){
					$("#recipes-button").css("visibility", 'hidden');
				}
			}
			if(hashMap.product!=undefined && hashMap.product!=null){
				
				//console.log("sucess got product length:"+hashMap.product.length);
				
				populateSearchDiv("productposts", $.i18n.prop('welcome.products'), true);
				for(var i=0;i<hashMap.product.length;i++){
					populateSearchPostsonGlobalSearchPage( hashMap.product[i], "productposts");
					//bindDishExploreEvents();
				}
				if(hashMap.product.length < 4){
					$("#productposts-button").css("visibility", 'hidden');
				}
			}
			if(hashMap.picture!=undefined && hashMap.picture!=null){
				
				//console.log("sucess got picture length:"+hashMap.picture.length);
				
				populateSearchDiv("pictures", $.i18n.prop('welcome.pictures'), true);
				for(var i=0;i<hashMap.picture.length;i++){
					populateSearchPostsonGlobalSearchPage( hashMap.picture[i], "pictures");
					//bindDishExploreEvents();
					/*$('.pictureitem').unbind('click');   	
			    	$('.heartLikePicture').unbind('click');  
			    	$('.pictureitem').click('click',initPitureItemsComponents);
			    	$('.heartLikePicture').on('click',trainPictureItemClick);*/
				}
				if(hashMap.picture.length < 4){
					$("#pictures-button").css("visibility", 'hidden');
				}
			}
			if(hashMap.video!=undefined && hashMap.video!=null){
				
				//console.log("sucess got video length:"+hashMap.video.length);
				
				populateSearchDiv("videos", $.i18n.prop('welcome.videos'), true);
				for(var i=0;i<hashMap.video.length;i++){
					populateSearchPostsonGlobalSearchPage( hashMap.video[i], "videos");
					//bindDishExploreEvents();
					/*$('.pictureitem').unbind('click');   	
			    	$('.heartLikePicture').unbind('click');  
			    	$('.pictureitem').click('click',initPitureItemsComponents);
			    	$('.heartLikePicture').on('click',trainPictureItemClick);*/
				}
				if(hashMap.video.length < 4){
					$("#videos-button").css("visibility", 'hidden');
				}
			}
			if(hashMap.person!=undefined && hashMap.person!=null){
				
				//console.log("sucess got person length:"+hashMap.person.length);
				
				populateSearchDiv("persons", $.i18n.prop('welcome.person'), true);
				for(var i=0;i<hashMap.person.length;i++){
					populateSearchPostsonGlobalSearchPage( hashMap.person[i], "persons");
				//	bindDishExploreEvents();
				}
				if(hashMap.person.length < 4){
					$("#persons-button").css("visibility", 'hidden');
				}
			}
			
			// Results in search response are populated and now binding their events
			bindDishExploreEvents();
			$('.pictureitem').unbind('click');   	
	    	$('.heartLikePicture').unbind('click');  
	    	$('.pictureitem').click('click',initPitureItemsComponents);
	    	$('.heartLikePicture').on('click',trainPictureItemClick);
			if (searchQueries.isDish == "true" && hashMap.dish == undefined){
				populateSearchDiv("dishes", $.i18n.prop('welcome.dishes'), false);
			}
			if (searchQueries.isRecipe == "true" && hashMap.recipe == undefined){
				populateSearchDiv("recipes", $.i18n.prop('welcome.recipes'), false);
			}
			if (searchQueries.isProduct == "true" && hashMap.product == undefined){
				populateSearchDiv("productposts", $.i18n.prop('welcome.products'), false);
			}
			if (searchQueries.isPicture == "true" && hashMap.picture == undefined){
				populateSearchDiv("pictures", $.i18n.prop('welcome.pictures'), false);
			}
			if (searchQueries.isVideo == "true" && hashMap.video == undefined){
				populateSearchDiv("videos", $.i18n.prop('welcome.videos'), false);
			}
			if (searchQueries.isPerson == "true" && hashMap.person == undefined){
				populateSearchDiv("persons", $.i18n.prop('welcome.person'), false);
			}
			
			
			$('.heartLikelogout').click(function(){
					$('#alertModel3Message').text('Please Login to Like this');
					$('#alertModal3').modal({

			            backdrop: 'static',
			            keyboard: false
			        });
				});
				

			$('.heartLike').unbind('click');   	

			$('.heartLike').on('click',trainClick);
			
			
		},
		error: function(data){
			//console.log("Error from response");
			$("#loadingPosts").hide();
			$('.toolbar').show();
		}
	});
}

//******************************* Populates dynamic POSTS **************************//

function populateSearchPostsonGlobalSearchPage(post, divName){
	if(post.category == "recipe" || post.category == "dish" || post.category == "product"){
	var html = "";
	
	   html = '<div class="col-sm-3 col-md-3 product rotation" post-id="'+post.postId+'">'
			+	'<div class="default">'
			+   '<a href="'+contextPath + post.postDetailURL+'" class="product-image">'
			+   '<input type="hidden" id="shareProductId" name="shareProductId"   ></input>'
			+   '<input type="hidden" id="departmentCategory" name="departmentCategory"   ></input>';
	   if(post.postOffer=='visible'){
		   html = html + '<span class="sale top" style="display:visible; !important "></span>';
	   }
	   
			console.log("Path pictres is: '+ CDNPath + CDNPath+contextPath+'/images_public/item_images/'+post.postPictureFront+'");
	   html = html+'<a href="'+contextPath+post.postDetailURL+'" class="product-image">'
			+		'<img class="lazy" src="'+CDNPath+contextPath+'/images_public/dummyimageforflipcard.jpg" data-original="'+CDNPath+contextPath+'/images_public/item_images/'+post.postPictureFront+'" onerror="this.src=\'https://placehold.it/270x270\';" alt="" title="" width="270" height="270"/>'
			+	  '</a>'
			+	    '<div class="myproduct-description">'
			+		'<div class="myvertical">'
			+		  '<h3  style="padding-top:15px;width:90% !important" class="product-name overFllowdotdot">'
			+			'<a href="'+contextPath+post.postDetailURL+'" >'+post.postTitle+'</a>'
			+		  '</h3>';
	   if(post.category!='recipe'){
		   var sharingPrice = "";
		   if(post.category == "dish"){
			   sharingPrice = $.i18n.prop('dishdetail.label.sharingcost');
		   }
		   html = html+ '<div class="price">'+sharingPrice+' '+post.postPrice+'</div>';
	   }
	   html = html+	 '</div>'
			+	  '</div>'
			+	'</div>'
				
			+	'<div class="product-hover">'
				
			+	 '<div style="width:70%; display:inline-block">' 
			+		'<a class="pin-footer-content" href="'+contextPath+'/profile/'+post.username+'/'+'">'
			+			'<img src="'+CDNPath+contextPath+'/images_public/'+post.userPicture+'" onError="this.src=\''+CDNPath+contextPath+'/images_public/defaultUserImage.jpg\';"   class="img-circle pin-image  "/>'
			+			'<a class="pin-title" href="'+contextPath+'/profile/'+post.username+'/'+'">'+post.userFullName+'</a>'
			+			'<div class="pin-title-job">'+post.userExpType+'</div>'
			+		'</a>'
			+	  '</div>'
				  
			/*+	  '<div class="btn-group sort-by btn-select pull-right hide-show-post">'
			+		  '<a class="btn dropdown-toggle btn-default filter-zindex" role="button" data-toggle="dropdown" href="#"><span class="caret"></span></a>'
			+		  '<ul class="dropdown-menu" style="width: 150px">'
			+			'<li><a href="#" class="reportContent" data-category="'+post.category+'" data-id="'+post.departmentId+'">Report Post</a></li>' 
			+		  '</ul>'
			+		'</div>'*/;
			if(post.postOffer=='visible'){
				html=html +  '<div style="height: 117px; ">';
			}else{
				html=html+ '<div style="height: 130px; ">';
			}
			
			html=html+	  '<a href="'+contextPath+post.postDetailURL+'" class="product-image" style="margin: 4px 0 0 15px">'
			+		'<img src="'+CDNPath+contextPath+'/images_public/item_images/'+post.postPictureBack+'" onerror="this.src=\'https://placehold.it/70x70\';" alt="" title="" width="70" height="70"/>'
			+	  '</a>'
				
			+	 '<ul>'
			+		'<li>'+post.keyPoint1+'</li>'
			+		'<li>'+post.keyPoint2+'</li>'
			+		'<li>'+post.keyPoint3+'</li>'
			+		'<li>'+post.keyPoint4+'</li>'
			+		'<li>'+post.keyPoint5+'</li>'
			+	  '</ul>'
			+		'</div>'	  
			+	 '<div class="reviews-box" style="margin-bottom: 10px; margin-top: 10px">'
			+		'<div class="rating-box">'
			+		  '<div style="width: '+post.averageReviews+'" class="rating">'
			+			'<svg xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" width="73px" height="12px" viewBox="0 0 73 12" enable-background="new 0 0 73 12" xml:space="preserve">'
			+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="6.5,0 8,5 13,5 9,7.7 10,12 6.5,9.2 3,12 4,7.7 0,5 5,5"></polygon>'
			+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="66.5,0 68,5 73,5 69,7.7 70,12 66.5,9.2 63,12 64,7.7 60,5 65,5 "></polygon>'
			+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="21.5,0 23,5 28,5 24,7.7 25,12 21.5,9.2 18,12 19,7.7 15,5 20,5 "></polygon>'
			+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="51.5,0 53,5 58,5 54,7.7 55,12 51.5,9.2 48,12 49,7.7 45,5 50,5 "></polygon>'
			+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="36.5,0 38,5 43,5 39,7.7 40,12 36.5,9.2 33,12 34,7.7 30,5 35,5 "></polygon>'
			+			'</svg>'
			+		  '</div>'
			+		'</div>'
			+		 '<span style="color: #fff">'+post.totalReviews+ ' '+ $.i18n.prop('request.send.reviewsss')+' </span>'
			+		 '<i class="fa fa-heart" style="color: #fff; margin-left: 20px">'
			+		 '</i>&nbsp';
			 if(post.postOffer=='visible'){
					/*html=html +		 '<span style="color: #fff" >'+ post.totalLikes+' </span>';*/
				 html=html +		 '<span style="color: #fff" id="totalLikes'+post.category+post.postId+'">'+ post.totalLikes+' </span>';
				}else{
					html=html +		 '<span style="color: #fff" id="totalLikes'+post.category+post.postId+'">'+ post.totalLikes+' </span>';
				}	
					
							
					html=html+	  '</div>'
				  
			+ 	  '<div>';
			
					//debugger;
					if(post.location[0] != null && post.location[0] != undefined){
						html=html+'<span style="margin:0 2 0 3;">'+$.i18n.prop('recipeitems.label.distance')+' </span>'
						+'<span> '+post.location[0].distance+'</span>'
						+       '<span  style="margin-left: 18px" >'+post.creationDate+'</span>'
						+	  '</div>';
					}
					else if(post.category == "dish" || post.category == "product"){
						html=html+		'<span style="margin:0 2 0 3;">'+$.i18n.prop('recipeitems.label.distance')+' </span>'
						+'<span> Not Available</span>'
						+'<span  style="margin-left: 18px" >'+post.creationDate+'</span>'
						+	  '</div>';
  
					}
					else
						html=html+ '<span>'+post.creationDate+'</span>'
						+	  '</div>'
	 
			
			if(post.postOffer=='visible'){
				html=html + '<div>Via <a href="'+contextPath+'profile/'+post.viaUserEmail+'/">'+post.viaUser+'</a></div>';
			}
			html=html+'<div class="actions" >';		
			if(post.likeUnlike=='0'){
				if(post.postOffer=='visible'){
					html=html+ '<a   href="'+contextPath+post.postDetailURL+'" title="Like"  class="add-wishlist " style="padding-top: 22px">';
					
				}else{
					html=html+ '<a   id="'+post.postId+'" data-category="'+post.category+'" title="Like"  class="add-wishlist heartLike " style="padding-top: 22px">';
				
				}
				
				html=html +' <svg  xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">'
				+'		  <path fill="#1e1e1e" d="M11.335,0C10.026,0,8.848,0.541,8,1.407C7.153,0.541,5.975,0,4.667,0C2.088,0,0,2.09,0,4.667C0,12,8,16,8,16'
				+'s8-4,8-11.333C16.001,2.09,13.913,0,11.335,0z M8,13.684C6.134,12.49,2,9.321,2,4.667C2,3.196,3.197,2,4.667,2C6,2,8,4,8,4'
				+'s2-2,3.334-2c1.47,0,2.666,1.196,2.666,2.667C14.001,9.321,9.867,12.49,8,13.684z"></path>'
				+'		  </svg>  '
				+'		</a>';
				
			}
            
	
				  
		
			html=html +  '<a href="'+contextPath+post.postDetailURL+'#reviews'+'" class="add-wishlist" style="padding-top: 12px">'
			+		 '<svg style="enable-background:new 0 0 500 500;" height="36px" id="Layer_1" version="1.1" viewBox="0 0 500 500" width="36px" xml:space="preserve" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">'
			+		  '<g>'
			+		  '<path d="M293.081,359.376l-63.02-42.148h-53.077c-17.084,0-30.984-14.352-30.984-31.994V173.82    c0-17.641,13.899-31.994,30.984-31.994h146.535c17.084,0,30.984,14.352,30.984,31.994v111.414'  
			+		    'c0,17.641-13.899,31.994-30.984,31.994h-30.437V359.376z M176.984,160.646c-7.035,0-12.758,5.91-12.758,13.174v111.414    c0,7.264,5.723,13.174,12.758,13.174h58.464l39.408,26.356v-26.356h48.663c7.035,0,12.758-5.91,12.758-13.174V173.82'  
			+		      'c0-7.264-5.723-13.174-12.758-13.174H176.984z"/>'
			+		  '</g>'
			+		  '</svg>'
			+		'</a>';
				  if(post.category!='recipe'){
					  html=html 	+		'<a href="'+contextPath+post.postDetailURL+'" class="add-cart my-icons" style="padding-top: 6px">'
						+		  '<sapn class="fa fa-shopping-cart grid-icons"></sapn>'
						+		'</a>'  ;
					  
				  }
			
					
		   html=html + '<a id="shareFlip" data-postTitle="'+post.postTitle+'" data-postDescription="'+post.postDescription+'" data-postUrl="'+post.postDetailURL+'" data-userFullName='+post.userFullName+' class="add-compare" data-imageUrl="'+CDNPath+contextPath+'/images_public/item_images/'+post.postPictureFront+'"     data-category="'+post.category+'" data-userFullName="'+post.userFullName+'" data-id="'+post.departmentId+'" >'
			+		  '<svg height="16px" id="Layer_1" style="enable-background:new 0 0 16 16;" version="1.1" viewBox="85 100 400 330" width="24px"' 
			+		    'xml:space="preserve" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">'
			+		    '<g><path d="M352,377.9H102.4V198.2h57.5c0,0,14.1-19.7,42.7-38.2H83.2c-10.6,0-19.2,8.5-19.2,19.1v217.9c0,10.5,8.6,19.1,19.2,19.1'
			+			'h288c10.6,0,19.2-8.5,19.2-19.1V288L352,319.4V377.9z M320,224v63.9l128-95.5L320,96v59.7C165.2,155.7,160,320,160,320'
			+			'C203.8,248.5,236,224,320,224z"/></g>'
			+		  '</svg>'
			+		'</a>'
					
			+	  '</div>'
			+	'</div>'
			+  '</div>';
	  
			
			$("."+divName).append(html);
	}
	else if(post.category == "picture" || post.category == "video"){
		var html = "";
		html=html+"<div style='cursor: pointer; cursor: hand;'  class='col-sm-3 col-md-3 product ' picturepost-id='"+post.postId+"' >"+
						"<div class='default'>"	;

		if(post.category  =="picture"){				
				html=html+	"<img class='pictureitem lazy' picturepost-id='"+post.postId+"' src='"+CDNPath+contextPath+"/images_public/dummyimageforflipcard.jpg' data-original='"+contextPath+"images_public/item_images/"+post.postPictureFront+"' alt='' title='' width='270' height='270'/>";
		}
			
		else if(post.category  == "video"){
				html=html+	"<img class='pictureitem lazy' picturepost-id='"+post.postId+"' src='"+CDNPath+contextPath+"/images_public/dummyimageforflipcard.jpg' data-original='"+contextPath+"images_public/item_images/"+post.postPictureFront+"' alt='' title='' width='270' height='270'/>"+
							"<span class ='video-span pictureitem' picturepost-id='"+post.postId+"' ></span>";
				 
		}
			
		html=html+			"<div class='myproduct-description pictureitem' picturepost-id='"+post.postId+"'>"
		+		"<div class='myvertical'>"
		+		  "<h3  style='padding-top:15px;width:90% !important' class='product-name overFllowdotdot'>"+post.postTitle+
			  
				
									"</h3>"+
			
								"</div><!-- 'vertical' -->"+
							"</div><!-- 'product-description' -->"+
						"</div><!-- 'default' -->"+
					"</div><!-- -->";
		$("."+divName).append(html);
	}
	else{
		var html = "";
		
		   html = '<div class="col-sm-3 col-md-3 product rotation" post-id="'+post.postId+'">'
				+	'<div class="default">'
				+   '<a href="'+contextPath+'profile/'+post.username+'" class="product-image">'
				+   '<input type="hidden" id="shareProductId" name="shareProductId"   ></input>'
				+   '<input type="hidden" id="departmentCategory" name="departmentCategory"   ></input>';
		   if(post.postOffer=='visible'){
			   html = html + '<span class="sale top" style="display:visible; !important "></span>';
		   }
		   
				
		   html = html+'<a href="'+contextPath+'profile/'+post.username+'/" class="product-image">'
				+		'<img style="width:270px !important;height:270px !important;" src="'+CDNPath+contextPath+'/images_public/'+post.userPicture+'" onError="this.src=\''+CDNPath+contextPath+'/images_public/defaultUserImage.jpg\';" alt="" title="" width="270" height="270"/>'
				+	  '</a>'
				+	  '<div class="myproduct-description">'
				+		'<div class="myvertical">'
				+		  '<h3  style="padding-top:15px;width:90% !important" class="product-name overFllowdotdot">'
				+			'<a href="'+contextPath+'profile/'+post.username+'/" >'+post.userFullName+'</a>'
				+		  '</h3>';
		   
		   html = html+	 '</div>'
				+	  '</div>'
				+	'</div>'
					
				+	'<div class="product-hover">'
					
				+	 '<div style="width:70%; display:inline-block">' 
				+		'<a class="pin-footer-content" href="'+contextPath+'/profile/'+post.username+'/'+'">'
				+			'<img src="'+CDNPath+contextPath+'/images_public/'+post.userPicture+'" onError="this.src=\''+CDNPath+contextPath+'/images_public/defaultUserImage.jpg\';"   class="img-circle pin-image  "/>'
				+			'<a class="pin-title" href="'+contextPath+'profile/'+post.username+'/'+'">'+post.userFullName+'</a>'
				+			'<div class="pin-title-job">'+post.userExpType+'</div>'
				+		'</a>'
				+	  '</div>'
					  
				/*+	  '<div class="btn-group sort-by btn-select pull-right hide-show-post">'
				+		  '<a class="btn dropdown-toggle btn-default filter-zindex" role="button" data-toggle="dropdown" href="#"><span class="caret"></span></a>'
				+		  '<ul class="dropdown-menu" style="width: 150px">'
				+			'<li><a href="#" class="reportContent" data-category="'+post.category+'" data-id="'+post.departmentId+'">Report Post</a></li>' 
				+		  '</ul>'
				+		'</div>'*/;
				if(post.postOffer=='visible'){
					html=html +  '<div style="height: 117px; ">';
				}else{
					html=html+ '<div style="height: 160px;">';
				}
				
				html=html+	  '<a href="'+contextPath+'profile/'+post.username+'/" class="product-image" style="margin: 4px 0 0 15px">'
				+		'<img style="width:70px !important;height:70px !important;" src="'+CDNPath+contextPath+'/images_public/'+post.userPicture+'" onError="this.src=\''+CDNPath+contextPath+'/images_public/defaultUserImage.jpg\';" alt="" title="" width="70" height="70"/>'
				+	  '</a>'
					
				+	 '<ul>'
				+		'<li>('+post.keyPoint1+') Followers</li>'
				+		'<li>('+post.keyPoint2+') Followings</li>'
				/*+		'<li>'+post.keyPoint3+'</li>'
				+		'<li>'+post.keyPoint4+'</li>'
				+		'<li>'+post.keyPoint5+'</li>'*/
				+	  '</ul>'
				+		'</div>'	  
				+	 '<div class="reviews-box" style="margin-bottom: 10px; margin-top: 10px; ">'
				+		'<div class="rating-box">'
				+		  '<div style="width: '+post.averageReviews+'" class="rating">'
				+			'<svg xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" width="73px" height="12px" viewBox="0 0 73 12" enable-background="new 0 0 73 12" xml:space="preserve">'
				+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="6.5,0 8,5 13,5 9,7.7 10,12 6.5,9.2 3,12 4,7.7 0,5 5,5"></polygon>'
				+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="66.5,0 68,5 73,5 69,7.7 70,12 66.5,9.2 63,12 64,7.7 60,5 65,5 "></polygon>'
				+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="21.5,0 23,5 28,5 24,7.7 25,12 21.5,9.2 18,12 19,7.7 15,5 20,5 "></polygon>'
				+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="51.5,0 53,5 58,5 54,7.7 55,12 51.5,9.2 48,12 49,7.7 45,5 50,5 "></polygon>'
				+				'<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="36.5,0 38,5 43,5 39,7.7 40,12 36.5,9.2 33,12 34,7.7 30,5 35,5 "></polygon>'
				+			'</svg>'
				+		  '</div>'
				+		'</div>'
				+		 '<span style="color: #fff">'+post.totalReviews+ ' '+ $.i18n.prop('request.send.reviewsss')+' </span>'
				+	'<div class="social" style="margin-top:10px">';
				if( post.userSocialContactMap != undefined &&  post.userSocialContactMap != null){
					if( post.userSocialContactMap.fb 		!= undefined && post.userSocialContactMap.fb != null ){
						var fburl = "";
						if (post.userSocialContactMap.fb.toLowerCase().indexOf("http") >= 0)
						{
							fburl = post.userSocialContactMap.fb;
						}else{
							fburl  = 'http://' + post.userSocialContactMap.fb;
						}
						html=html+		'<div class="item"><a style="margin: 0px 6px 0px 0px" class="sbtnf sbtnf-rounded color color-hover icon-facebook" href="'+fburl+'"></a></div>';
					}
					if( post.userSocialContactMap.tw 		!= undefined && post.userSocialContactMap.tw != null ){
						var twurl = "";
						if (post.userSocialContactMap.tw.toLowerCase().indexOf("http") >= 0)
						{
							twurl  = post.userSocialContactMap.tw;
						}else{
							twurl  = 'http://' + post.userSocialContactMap.tw;
						}
						html=html+		'<div class="item"><a style="margin: 0px 6px 0px 0px" class="sbtnf sbtnf-rounded color color-hover icon-twitter" href="'+twurl+'""></a></div>';
					}
					if( post.userSocialContactMap.lkn 		!= undefined && post.userSocialContactMap.lkn!= null ){
						var lknurl = "";
						if (post.userSocialContactMap.lkn.toLowerCase().indexOf("http") >= 0)
						{
							lknurl  = post.userSocialContactMap.lkn;
						}else{
							lknurl  = 'http://' + post.userSocialContactMap.lkn;
						}
						html=html+		'<div class="item"><a style="margin: 0px 6px 0px 0px" class="sbtnf sbtnf-rounded color color-hover icon-linkedin" href="'+lknurl+'"></a></div>';
					}
					if( post.userSocialContactMap.instagram != undefined && post.userSocialContactMap.instagram != null ){
						var instagramurl = "";
						if (post.userSocialContactMap.instagram.toLowerCase().indexOf("http") >= 0)
						{
							instagramurl  = post.userSocialContactMap.instagram;
						}else{
							instagramurl  = 'http://' + post.userSocialContactMap.instagram;
						}
						html=html+		'<div class="item"><a style="margin: 0px 6px 0px 0px" class="sbtnf sbtnf-rounded color color-hover icon-pinterest" href="'+instagramurl+'"></a></div>';
					}
					if( post.userSocialContactMap.pinterest != undefined && post.userSocialContactMap.pinterest != null ){
						var pinteresturl = "";
						if (post.userSocialContactMap.pinterest.toLowerCase().indexOf("http") >= 0)
						{
							pinteresturl  = post.userSocialContactMap.pinterest;
						}else{
							pinteresturl  = 'http://' + post.userSocialContactMap.pinterest;
						}
						html=html+		'<div class="item"><a style="margin: 0px 6px 0px 0px" class="sbtnf sbtnf-rounded color color-hover icon-instagram" href="'+pinteresturl+'"></a></div>';
					}
				}
				html=html+	'</div>';
				
				
		  
				
				$("."+divName).append(html);
	}
			

	    	
	    	
}


function populateSearchDiv(className, divTitle, isViewMoreButtonShow){
	var html = "";
	html = html + 
			'<div class="'+className+'" grid row">'
			+	'<div class="title-box entery-title" style="text-align: left;margin-left: 18px">'
			+		'<h1 class="title">'+divTitle+'</h1>'
			+	'</div>'
			+'</div><!-- .'+className+' -->';
			
	
	if(isViewMoreButtonShow != undefined && isViewMoreButtonShow != null && isViewMoreButtonShow != false){
		html = html +'<button type="button" id="'+className+'-button" class="btn btn-warning  btn-block">View More</button>'; 
		$(".products").append(html);
		$('#'+className+'-button').on('click',loadMoreButtonOnSearchButton);	
	}
	else{
		html = html + 
			'<div id = "sorry-'+className+'" class="description">'+$.i18n.prop('validation.sorrynomatchesfoundfor')+' "'+searchQueries.searchText+'" '+$.i18n.prop('validation.in')+' '+divTitle+'</div>';
		$(".products").append(html);
	}
}

//******************************* Define Load more Functionality of search **************************//

function loadMoreButtonOnSearchButton(button){
	if(button.target.id!=null && button.target.id != undefined){
		
		if(button.target.id == "productposts-button"){
			searchQueries.viewMoreSearchIn = "products";
			window.location.href = contextPath+"search/product?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
												"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText;
		}
		else if(button.target.id == "dishes-button"){
			searchQueries.viewMoreSearchIn = "dishes";
			window.location.href = contextPath+"search/dish?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
												"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText;
		}
		else if(button.target.id == "recipes-button"){
			searchQueries.viewMoreSearchIn = "recipes";
			window.location.href = contextPath+"search/recipe?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
												"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText;
		}
		else if(button.target.id == "pictures-button"){
			searchQueries.viewMoreSearchIn = "pictures";
			window.location.href = contextPath+"search/picture?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
												"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText;
		}
		else if(button.target.id == "videos-button"){
			searchQueries.viewMoreSearchIn = "pictures";
			window.location.href = contextPath+"search/video?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
												"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText;
		}
		else if(button.target.id == "persons-button"){
			searchQueries.viewMoreSearchIn = "recipes";
			window.location.href = contextPath+"search/person?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
												"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText;
		}
		
	}
	
}

function initViewMoreSearch(){

	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");

	$('.pagination .btn-group').children().hide();
	$('.pagination').append('<a class="btn loadmore" style="display:none;">Load More</a>');

	$('#catalog').append('<div id="loadMoreMsg"></div>');
	$('#endPosts').hide();
	$("#loadingPosts").show();
	
	var url = contextPath+"search/viewmore/"+searchQueries.viewMoreSearchIn+"?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
	"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText+"&mapLat="+searchQueries.latitude+"&mapLong="+searchQueries.longitude;
	addPriceSlideBar(searchQueries.startPrice, searchQueries.endPrice)
	fetchPagePosts(url, true);

	initPageCompenents();
	/* 
	
		Comment custom pagination of browser after inclusion of maps
		window.onpopstate = function(evt) {
		$('.products').infinitescroll('unbind');
		$('#endPosts').hide();
		// Grab the state from the event object
		var s = evt.state;
		// In webkit browsers the onpopstate event is called when page loads
		// but the event state is null at this time
		// This is also true when user hits back to page's initial state
		if (!s) {
			window.history.replaceState($(".products").html(), document.title, window.location.href);
			return;
		}
		$('#Slider2').off();
		$(".products").empty();
		$('.toolbar').empty();
		$('#starspan').text(s.searchQuery.rating);
	    $('#locationspan').text(s.searchQuery.distance);
		$(".products").append(s.posts);
		$('.toolbar').append(s.filter);
		$('#pricefilter').empty();
		s.searchQuery.rating;
		s.searchQuery.distance;
		
		s.searchQuery.endPrice
		addPriceSlideBar(s.searchQuery.startPrice, s.searchQuery.endPrice);
		searchQueries = s.searchQuery;
		nextPage = s.nextpage;
		//initPageCompenents();
		bindPageEvents();
		if(nextPage != ""){
			$('.products').infinitescroll('bind');
		}
		else{
			$('#endPosts').show();
		}
			
	}*/
		
}

function addPriceSlideBar(startP, endP){
//	var startP = s.searchQuery.startPrice;
//	var endP = s.searchQuery.endPrice;
	if(startP == "All")
		startP = 0;
	if(endP == "All")
		endP = 1000;
	var sliderhtml='<div class="price-regulator pull-right price-left">'+
						'<b>Price:</b>'+
						'<div class="layout-slider" style="margin-top: 4px">'+
							'<input id="Slider2" type="slider" name="price" value="'+startP+';'+endP+'" class="form-control"/>'+
						'</div>'+
					'</div>';
	$('#pricefilter').append(sliderhtml);
	$('#Slider2').slider({
		  from: 0,
		  to: 1000,
		  limits: false,
		//  heterogeneity: ['50/50000'],
		  step: 20,
		  dimension: '&nbsp;$',
		  values: [ 75, 300 ],
		  start: function( event, ui ) {
			  alert('Yessssssssssssssssssssssss');
		  }
		});
	 $('.jslider-pointer').html('\n\
				<svg xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 8 12" enable-background="new 0 0 8 12" xml:space="preserve">\n\
				  <path fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" d="M2,0h4c1.1,0,2,0.9,2,2l-2,8c-0.4,1.1-0.9,2-2,2l0,0c-1.1,0-1.6-0.9-2-2L0,2C0,0.9,0.9,0,2,0z"/>\n\
				</svg>\n\
			  ');
	 $('#Slider2').on( "slidden", function( event, value ) {	
			console.log('ENTER STATE CHANGE - value is : ' + value);
			var time = value.split(";");
			if(time.length == 2){
				
				searchQueries.startPrice = time[0];
				searchQueries.endPrice = time[1];
				//runSafely(globalSearch);
				$(".products").html();
				
				$(".products").empty();
				clearMarkers();
				//$('.products').infinitescroll('bind');
				$("#loadingPosts").show();
				$('#endPosts').hide();
				var url = contextPath+"search/viewmore/"+searchQueries.viewMoreSearchIn+"?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
				"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText+"&mapLat="+searchQueries.latitude+"&mapLong="+searchQueries.longitude;
				fetchPagePosts(url, false);
			}

		} );
}


function getNextPageViewMoreSearchURL(){
	return nextPage;
}

//******************************* Init Chefya Global Search Params **************************//
var state = {
		posts : "",
		searchQuery:{},
		nextpage : ""
};

//******************************* Init Search Filters Events **************************//
function initPageCompenents(){
		
 
	$("#staroptions li").on("click", function(e){
		   e.preventDefault();
		   //console.log("Stars "+$(this).text());
		   $('#starspan').text($(this).text());
		   searchQueries.rating = $(this).text();
		   $(".products").empty();
		   clearMarkers();
		   //$('.products').infinitescroll('bind');
		   $("#loadingPosts").show();
		   //$('#endPosts').hide();
		   var url = contextPath+"search/viewmore/"+searchQueries.viewMoreSearchIn+"?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
				"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText+"&mapLat="+searchQueries.latitude+"&mapLong="+searchQueries.longitude;
		   fetchPagePosts(url);
		   
	});	  
		
		
	$("#distanceoptions li").on("click", function(e){
		   e.preventDefault();
		   //console.log("Distance "+$(this).text());
		   $('#locationspan').text($(this).text());
		   if($(this).text() == "All")
			   searchQueries.distance = "ALL";   
		   else{
			   var distance = $(this).text().split("km");
			   if(distance.length <2){
				   distance = $(this).text().split("miles");
			   }
			   searchQueries.distance = distance[0].trim();
		   }
		   $(".products").empty();
		   clearMarkers();
		   //$('.products').infinitescroll('bind');
		   $("#loadingPosts").show();
		   $('#endPosts').hide();
		   var url = contextPath+"search/viewmore/"+searchQueries.viewMoreSearchIn+"?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
				"&location="+searchQueries.distance+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText+"&mapLat="+searchQueries.latitude+"&mapLong="+searchQueries.longitude;
		   fetchPagePosts(url);
	});	
	
	
	$(function () {
	    $(".timeline").show();
	    $(".message").hide();
	    $(".email").hide();
	});	
	
	$('input[type="radio"]').click(function(){
		 
		
	       
    	if($(this).attr("value")=="timeline"){
        	 $(".timeline").show();
        	 $(".message").hide();
        	 $(".email").hide();
        }
       
    	if($(this).attr("value")=="message"){    
        	var proname = $(".productname").text();        	
        	$(".shareproname").text(proname);
        	
        	var username = $(".username").text();        	
        	$(".shareusername").text(username);
        	        	
        	/*var desc = $(".description").text();        	
        	$(".sharedescription").text(desc);        */	
        	
        	 //alert("Text: " + username);
        	 $(".timeline").hide();
	       	 $(".message").show();
	       	 $(".email").hide();
	       	
	       	
        }
       
    	if($(this).attr("value")=="email"){
        	 $(".timeline").hide();
	       	 $(".message").hide();
	       	 $(".email").show();
        }
    
    });  
	
	/*..................................................Filters..............................  */
	  
	$("#filteroptions li").on("click", function(e){
		   e.preventDefault();
		   //console.log("Stars "+$(this).text());
		  
		   
		   $('#endPosts').hide();
		   var filterValue = $(this).text();
		   $('#filterspan').text(filterValue);
		   $(".products").empty();
		   $('.products').infinitescroll('bind');
		   fetchNewsFeedPosts(filterValue);
		   
	});
	
	$('button#submitReportContent').on("click",function(){
		//alert("report content 3 call");
		
		  var contentType=$("#contentType").val(); 
		  var contentId=$("#contentId").val();
		  var userMessage=$("#userMessage").val();
		  console.log(contentType);
		  console.log(contentId);
		  console.log(userMessage);
		  
		  var reportType = "";
		  var selected = $("input[type='radio'][name='optionsRadios']:checked");
		  if (selected.length > 0) {
			  reportType = selected.val();
		  }
		  console.log(reportType);  
		  var reportContentForm = {
	                "contentId": contentId,
	                "contentType": contentType,
	                "userMessage": userMessage,
	                "reportType": reportType
	              
	            };
		  $.ajax({
	            url:  contextPath+"/user/reportcontent",
	            type: "POST",
	            contentType: "application/json",
	            beforeSend: function(xhr) {  
                  xhr.setRequestHeader(header, token);  
              },
	            data: JSON.stringify(reportContentForm),
	           
	           success : function(data) {
	        	  //console.log("Success fully");
	        	  $("#reportModal").modal("hide");
	        	  $("#reportResponse").modal();
				
				},
				error : function(data){
				
					//console.log("Error recieved"+data);
				}
	        });
			 
		});
}

//******************************* Fetch posts for load more functionality **************************//
function fetchPagePosts(url_val, isOnInit){
	//$('.toolbar').hide();
	$.ajax({
		type : "GET",
		url : url_val,

		success : function(data) {
			//$('.toolbar').show();
			if(data.searchQueries != undefined){
				nextPage = data.searchQueries.nextpage;
				
				if(data.recipe != null)
					retList = data.recipe;
				else if(data.product != null)
					retList = data.product;
				else if(data.dish != null)
					retList = data.dish;
				else if(data.person != null)
					retList = data.person;
				else if(data.video != null)
					retList = data.video;
				else if(data.picture != null)
					retList = data.picture;
				//console.log("list of POsts fetched, length is : "+retList.length);
				for( var i = 0; i < retList.length; i++) {
					//console.log('user name is ' + data[i].postTitle);
					//populateNewsFeedPostsList(data[i]);
					populateSearchPostsonGlobalSearchPage(retList[i], "products");
				}
				if(searchQueries.viewMoreSearchIn == 'dish' ||searchQueries.viewMoreSearchIn == 'product'){
					showMapAndPins();
					addMarker();
				}
				
				$('.pictureitem').unbind('click');   	
		    	$('.heartLikePicture').unbind('click');  
		    	$('.pictureitem').click('click',initPitureItemsComponents);
		    	$('.heartLikePicture').on('click',trainPictureItemClick);
				$("#loadingPosts").hide();
				bindPageEvents();
	
				/*$('.products').infinitescroll(getLoading(), function(json, opts) {
					renderLoadMoreCallBack(json, opts);
				});*/
	
				if(retList.length < 12){
	
					//$('#endPosts').show();
					//console.log(' else part No More Posts Available : Unbinding Events ::');
					//$('.products').infinitescroll('unbind');
				}
				
				
				paginationDiv = "<div class='pagination-custom'>"
				+  "<div class='pagination-box pagination-centered'>"
				+    "<ul class='pagination pagination-lg'>";
				if(data.page.firstPage == true)
					paginationDiv = paginationDiv + "<li class='disabled '>";
				else
					paginationDiv = paginationDiv + "<li class='active'>";
				if(data.page.firstPage == true)
					paginationDiv = paginationDiv + "<span >← First</span>";
				else
					paginationDiv = paginationDiv + "<a class='active' onClick='loadNextPage(event)' url-attr='"+data.page.url+"&page=0&size="+data.page.size+"'>← First</a>";
				paginationDiv = paginationDiv +"</li>"; 
				if(data.page.hasPreviousPage == true)
					paginationDiv = paginationDiv + "<li class='active'>";
				else
					paginationDiv = paginationDiv + "<li class='disabled '>";
				
				if(data.page.hasPreviousPage == false)
					paginationDiv = paginationDiv + "<span >«</span>";
				else
					paginationDiv = paginationDiv +"<a class='active' onClick='loadNextPage(event)' url-attr='"+data.page.url+"&page="+((data.page.number)-2)+"&size="+data.page.size+"' title='Go to previous page'>«</a>"; 
				paginationDiv = paginationDiv +"</li>";
				
				for(var i=0;i<data.page.items.length;i++){
					if(data.page.items[i].current == true)
						paginationDiv = paginationDiv + "<li class='disabled '>";
					else
						paginationDiv = paginationDiv + "<li class='active'>";
					if(data.page.items[i].current == true)
						paginationDiv = paginationDiv + "<span >"+data.page.items[i].number+"</span>";
					else
						paginationDiv = paginationDiv + "<a class='active' onClick='loadNextPage(event)' url-attr='"+data.page.url+"&page="+((data.page.items[i].number)-1)+"&size="+data.page.size+"'><span text='"+data.page.items[i].number+"'>"+data.page.items[i].number+"</span></a>";
					paginationDiv = paginationDiv +"</li>";
				}
				if(data.page.hasNextPage == true)
					paginationDiv = paginationDiv + "<li class='active'>";
				else
					paginationDiv = paginationDiv + "<li class='disabled '>";
				if(data.page.hasNextPage == false)
					paginationDiv = paginationDiv + "<span >»</span>";
				else
					paginationDiv = paginationDiv + "<a onClick='loadNextPage(event)' url-attr='"+data.page.url+"&page="+((data.page.number))+"&size="+data.page.size+"' title='Go to next page'>»</a>"	
				paginationDiv = paginationDiv + "</li>";
				if(data.page.lastPage == true)
					paginationDiv = paginationDiv + "<li class='disabled '>";
				else
					paginationDiv = paginationDiv + "<li class='active'>";
				if(data.page.lastPage == true)
					paginationDiv = paginationDiv + "<span >Last →</span>";
				else
					paginationDiv = paginationDiv + "<a class='active' onClick='loadNextPage(event)' url-attr='"+data.page.url+"&page="+(data.page.totalPages-1)+"&size="+data.page.size+"'>Last →</a>"

				paginationDiv = paginationDiv + "</li>";
				
				paginationDiv = paginationDiv 
				+    "</ul>"
				+  "</div>"
				+"</div>";
				$("#catalog").append(paginationDiv);
			}
			else{
				$("#loadingPosts").hide();
				
			}
			
		},
		error: function(data){
			////console.log("Error from response");
			$('.toolbar').show();
		}
	});
}

function loadNextPage(element){
	var url = element.currentTarget.getAttribute("url-attr");
	//alert(url);
	url = url + "&mapLat="+searchQueries.latitude+"&mapLong="+searchQueries.longitude;
	if(url != null && url != ""){
		$(".products").empty();
		clearMarkers();
		$("#loadingPosts").show();
		 fetchPagePosts(url);
	}
}

//******************************* Handle Load more callback **************************//
function renderLoadMoreCallBack(json, opts){
	//debug("Object is "+ JSON.stringify(json));
	var obj = jQuery.parseJSON(JSON.stringify(json));
	//count=obj.length;
	var retList;
	if(obj.recipe != null)
		retList = obj.recipe;
	else if(obj.product != null)
		retList = obj.product;
	else if(obj.dish != null)
		retList = obj.dish;
	else if(obj.person != null)
		retList = obj.person;
	else if(obj.video != null)
		retList = obj.video;
	else if(obj.picture != null)
		retList = obj.picture;
	nextPage = obj.searchQueries.nextpage;
	if(retList.length > 0){
		//var retList = obj[1];
		for( var i = 0; i < retList.length; i++) {
			//console.log('user name is ' + data[i].postTitle);
			populateSearchPostsonGlobalSearchPage(retList[i], "products");
			//populateNewsFeedPostsList(retList[i]);
			
		} 
		$('.heartLikelogout').click(function(){
			$('#alertModel3Message').text($.i18n.prop('newsfeed.loginlike'));
			$('#alertModal3').modal({

	            backdrop: 'static',
	            keyboard: false
	        });
		});
		

		$('.heartLike').unbind('click');    	

		$('.heartLike').on('click',trainClick);
		bindPageEvents();
		
    }else{
		$('#endPosts').show();
		console.log('No More Posts Available : Unbinding Events ::');
		$('.products').infinitescroll('unbind');
	}
	
}
function getLoading(){
	return {
		 
		 loading: {
	            finished: newsFeedLoaded,
	            finishedMsg: "",
				img: contextPath + "/img/loading.gif",
	            msgText: "<p>"+$.i18n.prop('validation.loadingmorepost')+"</p>",
	            selector:"#loadMoreMsg",
	            speed: 'fast'
	        },
			navSelector  	: ".pagination",
			nextSelector 	: ".pagination a",
			itemSelector 	: "#catalog .product",
			contentSelector: ".products",
			debug		 	: false,
			dataType	 	: 'json',
			behavior		: 'twitter',
			path: getNextPageViewMoreSearchURL,
			appendCallback	: false // USE FOR PREPENDING
	    };
}

function bindPageEvents(){
	//Product
	$(function() {
	    $("img.lazy").lazyload();
	});
	  if(!navigator.userAgent.match(/iPad|iPhone|Android/i)) {
		$('.product, .employee')
		  .hover(function(event) {
			event.preventDefault();
			
			$(this).addClass('hover');
		  }, function(event) {
			event.preventDefault();
			
			$(this).removeClass('hover');
		  });
	  }
	  
	  $('body').on('touchstart', function (event) {
		event.stopPropagation();
		
		if ($(event.target).parents('.product, .employee').length==0) {
	      $('.product, .employee').removeClass('hover');
	    }
	  });

	  $('.product, .employee').on('touchend', function(event){
		if ($(this).hasClass('hover')) {
		  $(this).removeClass('hover');
		} else {
		  $('.product, .employee').removeClass('hover');
		  $(this).addClass('hover');
		}
	  });
	  
	  $('a#shareFlip').on("click",function(){
		category=$(this).data("category"); 
		itemId=$(this).data("id");


		$(".sharedescription").text( $(this).attr('data-postDescription')); 
		$("#shareModelTitle").text($.i18n.prop('model.share.sharing')+" "+$.i18n.prop(category)+" '"+ $(this).attr('data-postTitle')+"'");  
		console.log(name);
		console.log(category);
		console.log(itemId);
		document.getElementById("shareImageUrl").src=$(this).attr('data-imageUrl');
		var name=$(this).data("userFullName");
		document.getElementById("userFullName").value=name;
		postUrl=$(this).attr('data-postUrl');
		$(".shareproname").text($(this).attr('data-postTitle'));
		$(".shareusername").text(' '+$(this).attr('data-userFullName'));
		$("#shareModal").modal();
			 
		});
	  
	  
	  $('a#retweet').on("click",function(){
		  //console.log("adsfasdfdsfasdfasd");
		  category=$(this).data("category"); 
		  itemId=$(this).data("id");
		
		  if(category=='dish'){
			  $.ajax({
				  type : "GET",
				  url : contextPath+"/user/getDishSharedUser/"+itemId,
				  success : function(data) {
					  $("#retweeterModal").modal();  
					 
						$("#retweeterModalBody").empty();
					  //console.log("list of followers fetched, length is : "+data.length);
					  for( var i = 0; i < data.length; i++) {
						  populateSharedUserList(data[i]);
					  } 
				  },
				  error: function (xhr, ajaxOptions, thrownError) {
	    		        if(xhr.status==403) {
	    		        	window.location.href = contextPath+"login";
	    		        }
	    		      }
			
			 
			  });
    		
		  }
		  else if(category=="product"){
			  $.ajax({
				  type : "GET",
				  url : contextPath+"/user/getProductSharedUser/"+itemId,
				  success : function(data) {
					  $("#retweeterModalBody").empty();
					  $("#retweeterModal").modal();  
					  //console.log("list of followers fetched, length is : "+data.length);
					  for( var i = 0; i < data.length; i++) {
						  populateSharedUserList(data[i]);
					  } 
				  },
				  error: function(data){
					  //console.log("Error from response");
				  }
			
			 
			  });
    		
		  }
	  });
	  
	  function populateSharedUserList(dataList){
			var html = '<div class="col-sm-8 col-md-8 respond" style=" width: 100%; !important">' 
			+	'<div class="name">'
			+		'<a style="text-decoration:none" href="'+contextPath+'profile/'+dataList.userName+'/'+'">'
				+		'<div class="icon">'
				+ 			'<img class="img-circle" src="'+contextPath+'images_public/'+dataList.profileImage+'" onError="this.src=\''+contextPath+'images_public/defaultUserImage.jpg\';" alt="" style="height: 35px;width:35px;">'
				+		'</div>'
				+		'<strong  style="padding-left: 10px;">'+dataList.userFirstName+' '+dataList.userLastName+' at '+dataList.sharingDate+'</strong>'
			+		'</a>'
			+	'</div>'
			+'</div>';
			
			$("#retweeterModalBody").append(html);
		}	 
	 
	  
	  $('a.reportContent').on("click",function(){
			
		  var contentType=$(this).data("category"); 
		  var contentId=$(this).data("id");
		  $("#contentType").val(contentType);
		  $("#contentId").val(contentId);
		  $("#userMessage").val("");
		  $("#reportModal").modal();			 
		});
	  
 }

function initPictuteItems(){
	$('.pagination ul').children().hide();
	$('.pagination').append('<a class="btn loadmore" style="display:none;">Load More</a>');
	$('#catalog').append('<div id="loadMoreMsg"></div>');
	$('#endPosts').hide();
	
	//bindDishExploreEvents();
	
	$('.products').infinitescroll('bind');
	$('.pictureitem').click('click',initPitureItemsComponents);
	
	

	$('.heartLikePicture').on('click',trainPictureItemClick);
	 
//	 $('.pictureitem').unbind('click');   	
//	$('.heartLike').unbind('click');  
	$('.products').infinitescroll({
		 
		 loading: {
	            finished: newsFeedLoaded,
	            finishedMsg: $.i18n.prop('newsfeed.nomorepost'),
				img: contextPath + "/img/loading.gif",
	            msgText: "<p>"+$.i18n.prop('validation.loadingmorepost')+"</p>",
	            selector:"#loadMoreMsg",
	            speed: 'fast'
	        },
			navSelector  	: ".pagination",
			nextSelector 	: ".pagination a#nextPage",
			itemSelector 	: "#catalog .product",
			contentSelector: ".products",
			debug		 	: true,
			//dataType	 	: 'json',
			behavior		: undefined,
			path: undefined, 
			dataType: 'html',
			appendCallback: true// USE FOR PREPENDING
	    }, function(json, opts) {
	    	
	    	 
	    	 $('.pictureitem').unbind('click');   	
	    	$('.heartLike').unbind('click');  
	    	$('.pictureitem').click('click',initPitureItemsComponents);
	    	
	    	

	    	$('.heartLikePicture').on('click',trainPictureItemClick);
	    	
	    });
}


//******************************* Get related posts to the posts in search **************************//
function getRelatedPosts(postId, postName, postDescription, key1, key2, key3, key4, key5, postType){
	//alert(postName+ postDescription+ key1+ key2+ key3+ key4+ key5+ postType);
	var searchRelatedPost = {postId:postId, postName:postName, postDescription:postDescription, key1:key1,	key2:key2,
			key3:key3, key4:key4, key5:key5, postType:postType};
	var token = $("meta[name='_csrf']").attr("content");
   	var header = $("meta[name='_csrf_header']").attr("content");

   	setTimeout( function() {

   		$.ajax({
   			type : "POST",
   			contentType: "application/json",
   			url : contextPath+"getRelatedPosts",
   			beforeSend: function(xhr) {  
   	            xhr.setRequestHeader(header, token);  
   	        },
   			data: JSON.stringify(searchRelatedPost),
   			
   			success : function(retList) {
   				
   				
   				for(var i =0; i<retList.length; i++){
   					/*postName: "zxczxc"
   						postUrl: "/product/Chinese/5/zxczxc/25"
   						price: "12.0"
   						rating*/
   					var name = retList[i].postName;
   					if(retList[i].postName.length > 15){
   						name = retList[i].postName.substring(0,14);
   						name = name + "...";
   					}
   					var html = '<li class="clearfix">'+
   					  '<a class="product-image" href="'+contextPath+retList[i].postUrl+'">'+
   						'<img width="72" height="72" title="" alt="" src="'+contextPath+'images_public/item_images/'+retList[i].picturUrl+'"/>'+
   					  '</a>'+
   					  '<h3 class="product-name">'+
   						'<a href="'+contextPath+retList[i].postUrl+'">'+name+'</a>'+
   					  '</h3>'+
   					  '<div class="rating-box">'+
   						'<div class="rating" style="width: '+retList[i].rating+'">'+
   						  '<svg xml:space="preserve" enable-background="new 0 0 73 12" viewBox="0 0 73 12" height="12px" width="73px" y="0px" x="0px" xmlns:xlink="https://www.w3.org/1999/xlink" xmlns="https://www.w3.org/2000/svg">'+
   							 '<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="6.5,0 8,5 13,5 9,7.7 10,12 6.5,9.2 3,12 4,7.7 0,5 5,5" class="orange"/>'+
   							 '<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="66.5,0 68,5 73,5 69,7.7 70,12 66.5,9.2 63,12 64,7.7 60,5 65,5 " class="orange"/>'+
   							 '<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="21.5,0 23,5 28,5 24,7.7 25,12 21.5,9.2 18,12 19,7.7 15,5 20,5 " class="orange"/>'+
   							 '<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="51.5,0 53,5 58,5 54,7.7 55,12 51.5,9.2 48,12 49,7.7 45,5 50,5 " class="orange"/>'+
   							 '<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#1e1e1e" points="36.5,0 38,5 43,5 39,7.7 40,12 36.5,9.2 33,12 34,7.7 30,5 35,5 " class="orange"/>'+
   						  '</svg>'+
   						'</div>'+
   					  '</div>';
   					  if(postType != "recipe"){
   						  html = html+'<div class="price-box">'+				
   							'<span class="price">'+retList[i].price+'</span>'+
   						 '</div>';
   						}
   					html = html+'</li>';
   					$("#relatedposts").append(html);
   				}
   			},
   			error: function(data){
   				//console.log("Error from response");
   				
   			}
   		});
	}, 3000 );
	
	
}

//******************************* Init Chefya Global Search Params Object**************************//
function searchglobal(){
	/*if($("#searchText").val() == undefined || $("#searchText").val() == null || $("#searchText").val().trim() == ""){
		//alert("PLease enter some text to search");
	}else{*/
		/*$("#isRecipe")
		$("#isPicture")
		$("#isVideo")
		$("#isRecipe")
		$("#isProduct")
		$("#isDish")*/
		/*window.location.href = contextPath+"search/"+searchQueries.searchText+"?item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
			"&location=5&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText;*/
		var isProduct = isRecipe = isDish = isVideo = isPicture = isPerson = null;
		if ($('#isProduct').is(":checked"))
		{
			isProduct = "isProduct=true"
		}
		if ($('#isRecipe').is(":checked"))
		{
			isRecipe = "isRecipe=true"
		}
		if ($('#isDish').is(":checked"))
		{
			isDish = "isDish=true"
		}
		if ($('#isVideo').is(":checked"))
		{
			isVideo = "isVideo=true"
		}
		if ($('#isPicture').is(":checked"))
		{
			isPicture = "isPicture=true"
		}
		if ($('#isPerson').is(":checked"))
		{
			isPerson = "isPerson=true"
		}
		var url =  contextPath+"searchby/"+$("#searchText").val().trim();
		var isFirstParameterFound = false;
		if(isProduct != null){
			url += "?"+isProduct;
			isFirstParameterFound = true;
		}
		if(isRecipe != null){
			if(!isFirstParameterFound){
				url += "?"+isRecipe;
				isFirstParameterFound = true;

			}
			else
				url += "&"+isRecipe;
		}

		if(isDish != null){
			if(!isFirstParameterFound){
				url += "?"+isDish;
				isFirstParameterFound = true;
			}
			else
				url += "&"+isDish;
		}
		if(isVideo != null){
			if(!isFirstParameterFound){
				url += "?"+isVideo;
				isFirstParameterFound = true;
			}
			else
				url += "&"+isVideo;
		}
		if(isPicture != null){
			if(!isFirstParameterFound){
				url += "?"+isPicture;
				isFirstParameterFound = true;
			}
			else
				url += "&"+isPicture;
		}
		if(isPerson != null){
			if(!isFirstParameterFound){
				url += "?"+isPerson;
				isFirstParameterFound = true;
			}
			else
				url += "&"+isPerson;
		}
		window.location.href = url;
	//}

}

//******************************* Init Posts' Map module **************************//
var locationName, locationLatitude, locationLongitude;
function initMapsPopUp(loggedinlocation, country_code, latitude, longitude){
	$("#location-name").text(loggedinlocation);
	$("#location-icon").on("click", function(e){
		openLocationModel();
	});	
	function openLocationModel(){	
		$('#locationModel').modal({
			
			backdrop: 'static',
			keyboard: false
		});
		if(map == null)
			initialize();
		else
			initialized();
		setTimeout(function() {
			google.maps.event.trigger(map, "resize");
			map.setCenter(new google.maps.LatLng(latitude,longitude));
			map.setZoom( 8);
	    }, 1000);
	}
	// This example uses the autocomplete feature of the Google Places API.
	// It allows the user to find all hotels in a given place, within a given
	// country. It then displays markers for all the hotels returned,
	// with on-click details for each hotel.
	
	var map, places, infoWindow;
	var markers = [];
	var autocomplete;
	
	var countryRestrict = { 'country': country_code };
	var MARKER_PATH = 'https://maps.gstatic.com/intl/en_us/mapfiles/marker_green';
	var hostnameRegexp = new RegExp('^https?://.+?/');
	
	
	function initialize() {
	
		var myOptions = {
			center: new google.maps.LatLng(latitude,longitude),
			zoom: 3
		};
		
		map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
		
		infoWindow = new google.maps.InfoWindow({
		    content: document.getElementById('info-content')
		});
		
		// Create the autocomplete object and associate it with the UI input control.
		// Restrict the search to the default country, and to place type "cities".
		
		autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
		{
			//types: ['(regions)'],
		    //types: ['(regions)'],
		    componentRestrictions: countryRestrict
		});
		places = new google.maps.places.PlacesService(map);
		
		google.maps.event.addListener(autocomplete, 'place_changed', onPlaceChanged);
		
	}
	function initialized(){
		map.panTo(new google.maps.LatLng(latitude, longitude));
	}
	
	// When the user selects a city, get the place details for the city and
	// zoom the map in on the city.
	
	function onPlaceChanged() {
	  var place = autocomplete.getPlace();
	  if (place.geometry) {
	    map.panTo(place.geometry.location);
	    map.setZoom(15);
		clearMarkers();
		addMarker(place)
		
	  } else {
	    document.getElementById('autocomplete').placeholder = $.i18n.prop('validation.enteraddress');
	  }
	
	}
	function addMarker(place){
		markers[0] = new google.maps.Marker({
			position: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
			map: map
		});
		locationName = place.formatted_address;
		locationLatitude = place.geometry.location.lat();
		locationLongitude = place.geometry.location.lng();
			
	}
	
	function clearMarkers() {
	  for (var i = 0; i < markers.length; i++) {
	    if (markers[i]) {
	      markers[i].setMap(null);
	    }
	  }
	  markers = [];
	}
	
	
}
function saveLocation(){
	
	$.ajax({
        url:  contextPath+"/submitlocation",
        type: "GET",
        contentType: "application/json",
        
        data: {
        	"latitude" 		: locationLatitude,
        	"longitude"		: locationLongitude,
        	"locationname" 	: locationName
        	//"country_code"	: country_code
        },
       
       success : function(data) {
    	  	//console.log("Success fully"+data);
    	  	$("#location-name").text(locationName);
    	  	location.reload();
		},
		error : function(data){
		
			//console.log("Error recieved"+data);
		}
    });
}