package com.arc.chefya.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.Point;
import org.springframework.data.solr.core.geo.Distance;

import com.arc.chefya.Pojo.Product;
import com.arc.chefya.document.PostDocument;
import com.arc.chefya.forms.SearchQueries;


public interface PostIndexService {

    public void addToIndex(PostDocument todoEntry);

    public void deleteFromIndex(String id);

    public List<PostDocument> search(String searchTerm);
    
    public void searchByPoint();
    
    public void searchWithinDistance(Point point, String postType, Distance distance, String strStringSearch);
    
    public void searchWithinDistanceAndType(Point point, Distance distance, String strStringSearch, String postType);
    
	public List<PostDocument> globalSearch(String searchText, ArrayList<String> postType);
	
	public void updateNumberOfLikes(String postUniqueId, String numberofLikes);

	public Page<PostDocument> ExploreDishes(String categoryId, String timeZone, Pageable pageable);
	
	public Page<PostDocument> searchFourPostsByPostType(String postType, SearchQueries searchQueries, String timeZone);

	public Page<PostDocument> ExploreProducts(String categoryId, String timeZone, Pageable pageable);

	public Page<PostDocument> ExploreRecipes(String categoryId, Pageable pageable);
	
	public Page<PostDocument> viewMore(String searchText, String postType, Pageable p);

	public Page<PostDocument> viewMoreInDept(SearchQueries searchQueries, String postType,  String timeZone, Pageable p);

	public void updateNumberOfRatingsAndAverageRating(String postUniqueId, long longValue, float averageRating);
	
	public void updateInIndex(PostDocument postDocument);
	
	public void updateAllPostsOfUser(List<PostDocument> listPostDocument);

	public Page<PostDocument> ExplorePictures(String categoryId, Pageable pageable);

	public Page<PostDocument> ExploreVideos(String categoryId, Pageable pageable);
	
	public List<PostDocument> getRelatedPosts(String postId, String postName, String postDescription, List<String> postKeyPoints,
			String postType, String countryCode, String timeZone);

	void hideFromIndex(String id);

	void updateFollowFollowingUsers(List<PostDocument> listPostDocument);

	public PostDocument findBySolrPostId(String postId);

	public void updateAllStatusOfUser(List<PostDocument> listPostDocument);

}