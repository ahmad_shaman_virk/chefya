package com.arc.chefya.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.geo.Point;
import org.springframework.data.solr.core.SolrOperations;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.geo.Distance;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arc.chefya.document.PostDocument;
import com.arc.chefya.forms.SearchQueries;
import com.arc.chefya.solr.dao.ProductDocumentDao;
import com.arc.chefya.util.Constants;
import com.arc.chefya.util.HelpingClass;

@SuppressWarnings("deprecation")
@Service
public class PostIndexServiceImpl implements PostIndexService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PostIndexServiceImpl.class);

	protected static final String QUERY_METHOD_METHOD_NAME = "methodName";
	protected static final String QUERY_METHOD_NAMED_QUERY = "namedQuery";
	protected static final String QUERY_METHOD_QUERY_ANNOTATION = "queryAnnotation";

	@Resource
	private ProductDocumentDao repository;

	@Resource
	private SolrTemplate solrTemplate;
	
	@Autowired
	SolrOperations solrOperations;
	
	@Autowired
	HttpSession httpSession;

	@Transactional
	@Override
	public void addToIndex(PostDocument postEntry) {
		LOGGER.debug("Saving document with information: {}", postEntry);

		repository.save(postEntry);
	}

	@Transactional
	@Override
	public void deleteFromIndex(String id) {
		LOGGER.debug("Deleting an existing document with id: {}", id);
		repository.delete(id);
	}
	
	@Transactional
	@Override
	public void hideFromIndex(String id) {
		LOGGER.debug("hide a document with id: {}", id);
		repository.setPostStatusHide(id);
	}

	@Override
	public List<PostDocument> search(String searchTerms) {
		LOGGER.debug("Searching documents with search term: {}", searchTerms);
		return null;//findDocuments(searchTerms);
	}

	private Sort sortByIdDesc() {
		return new Sort(Sort.Direction.DESC, PostDocument.POST_ID);
	}

	@Override
	public void searchByPoint() {
		//		repository.findByListLocationPointsWithin(p, d)

	}

	@Override
	public void searchWithinDistance(Point point, String postType, Distance distance,
			String strStringSearch) {

		List<PostDocument> list = repository.findByListLocationPointsWithin(point, distance);
		LOGGER.debug("searchWithinDistance method result: {}",list);
	}

	@Override
	public void searchWithinDistanceAndType(Point point, Distance distance, String strStringSearch, String postType) {
		LOGGER.debug("searchWithinDistanceAndType method parameters: {}, postype",postType);
		//		List<PostDocument> list = repository.findByListLocationPointsWithinAndPostType(point, distance, postType);
		List<PostDocument> list = repository.findAllFilterPostTypeAndListLocationPointsWithin(point, distance, postType);
		LOGGER.debug("searchWithinDistanceAndType method result: {}",list);

	}

	@Override
	public List<PostDocument> globalSearch(String searchText, ArrayList<String> postType) {
		List<String> searchList = Arrays.asList(searchText.split("\\s+"));
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		LOGGER.debug("\n \n searchList {} --- postType {}",searchList,postType);
		
		StringBuilder keywords = new StringBuilder();
		
		for (Iterator<String> iterator = searchList.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			keywords.append("*" + string + "*" + " ");
		}
		
		List<PostDocument> results = repository.findAllFilterPostTypeContains( keywords.toString(), postType);
		LOGGER.debug("--------------------------------------------------"
				+ "\n \n findAllFilterPostTypeContainsDesrciption postname result.size {}", results.size());
		for(PostDocument result: results){
			LOGGER.debug("\nresult search result name: {} | des: {} | key : {} | type: {} | id: {}", 
					result.getPostName(), result.getPostDescription(),
					result.getKeyPoints(), result.getPostType(), result.getPostId());
		}
		
		return null;
	}
	
	@Override
	public Page<PostDocument> ExploreDishes(String categoryId, String timeZone,
			Pageable pageable) {
		//PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(new Order(Direction.DESC, PostDocument.CREATION_DATE)));
		ArrayList<Order> orderList = new ArrayList<Sort.Order>();
		Order order1 = new Order(Direction.DESC, PostDocument.CREATION_DATE);
		Order order2 = new Order(Direction.DESC, PostDocument.RATING);
		orderList.add(order2);
		orderList.add(order1);
		Date currentDate = HelpingClass.getTodayDateWithoutTimeInDateObj(timeZone);
		PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(orderList));
		LOGGER.info("getPageNumber : "+pageRequest.getPageNumber()+" offset"+pageRequest.getOffset()+" pagesize"+pageRequest.getPageSize());
		Page<PostDocument> p ;
		if(httpSession.getAttribute(Constants.IS_LOCATION_FOUND) != null && 
				!httpSession.getAttribute(Constants.IS_LOCATION_FOUND).equals("false")){
			
			Point point = new Point(Double.parseDouble((String) httpSession.getAttribute("userLat")),
					Double.parseDouble((String) httpSession.getAttribute("userLong")));
			Distance distance = PostDocument.MAX_DISTANCE;
			String countryCode = "*";
			if(httpSession.getAttribute("country_code")!=null)
				countryCode = (String) httpSession.getAttribute("country_code");
			p = repository.findByPostTypeAndSubCategoryWithLocation(PostDocument.DISH, categoryId, point, distance, countryCode, currentDate, pageRequest);
		}
		else
			p = repository.findByPostTypeAndSubCategory(PostDocument.DISH, categoryId, pageRequest);
		
		for(PostDocument result: p){
			LOGGER.debug("\nresult search result distance:{}, name: {} | des: {} | key : {} | type: {} | id: {}", 
					result.getScore(), result.getPostName(), result.getPostDescription(),
					result.getKeyPoints(), result.getPostType(), result.getPostId());
		}
		return p;

	}
	
	@Override
	public void updateNumberOfLikes(String postUniqueId, String numberofLikes) {
		repository.updateNumOfLikes( postUniqueId,  numberofLikes);
	}
	
	@Override
	public Page<PostDocument> ExploreProducts(String categoryId, String timeZone, Pageable pageable) {
		LOGGER.debug("Enter in ExploreProducts()");
		ArrayList<Order> orderList = new ArrayList<Sort.Order>();
		Order order1 = new Order(Direction.DESC, PostDocument.CREATION_DATE);
		Order order2 = new Order(Direction.DESC, PostDocument.RATING);
		orderList.add(order2);
		orderList.add(order1);
		Date currentDate = HelpingClass.getTodayDateWithoutTimeInDateObj(timeZone);
		PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(orderList));
//		PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(new Order(Direction.DESC, PostDocument.CREATION_DATE)));
		LOGGER.info("getPageNumber : "+pageRequest.getPageNumber()+" offset"+pageRequest.getOffset()+" pagesize"+pageRequest.getPageSize());
		Page<PostDocument> p ;
		if(httpSession.getAttribute(Constants.IS_LOCATION_FOUND) != null && 
				!httpSession.getAttribute(Constants.IS_LOCATION_FOUND).equals("false")){
			
			Point point = new Point(Double.parseDouble((String) httpSession.getAttribute("userLat")),
					Double.parseDouble((String) httpSession.getAttribute("userLong")));
			Distance distance = PostDocument.MAX_DISTANCE;
			String countryCode = "*";
			if(httpSession.getAttribute("country_code")!=null)
				countryCode = (String) httpSession.getAttribute("country_code");
			p = repository.findByPostTypeAndSubCategoryWithLocation(PostDocument.PRODUCT, categoryId, point, distance, countryCode, currentDate, pageRequest);
		}
		else
			p = repository.findByPostTypeAndSubCategory(PostDocument.PRODUCT, categoryId, pageRequest);
//		Page<PostDocument> p = repository.findByPostTypeAndSubCategoryWithLocation(PostDocument.DISH, categoryId, pageRequest);
		for(PostDocument result: p){
			LOGGER.debug("\nresult search result distance:{}, name: {} | des: {} | key : {} | type: {} | id: {}", 
					result.getScore(), result.getPostName(), result.getPostDescription(),
					result.getKeyPoints(), result.getPostType(), result.getPostId());
		}
		
		
		LOGGER.debug("Exit :: ExploreProducts()");

		return p;
		
		
		
	}

	@Override
	public Page<PostDocument> ExploreRecipes(String categoryId, Pageable pageable) {
		LOGGER.debug("Enter in ExploreRecipes()");
		ArrayList<Order> orderList = new ArrayList<Sort.Order>();
		Order order1 = new Order(Direction.DESC, PostDocument.CREATION_DATE);
		Order order2 = new Order(Direction.DESC, PostDocument.RATING);
		orderList.add(order2);
		orderList.add(order1);
		PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(orderList));
		//PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(new Order(Direction.DESC, PostDocument.CREATION_DATE)));
		LOGGER.info("getPageNumber : "+pageRequest.getPageNumber()+" offset"+pageRequest.getOffset()+" pagesize"+pageRequest.getPageSize());
		Page<PostDocument> p = repository.findByPostTypeAndSubCategoryAndPostStatus(PostDocument.RECIPE, categoryId, "1", pageRequest);
		LOGGER.debug("Exit :: ExploreRecipes()");
		return p;
	}
	
	@Override
	public Page<PostDocument> ExplorePictures(String categoryId, Pageable pageable) {
		LOGGER.debug("Enter in ExplorePictures()");
		PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(new Order(Direction.DESC, PostDocument.CREATION_DATE)));
		LOGGER.info("getPageNumber : "+pageRequest.getPageNumber()+" offset"+pageRequest.getOffset()+" pagesize"+pageRequest.getPageSize());
		Page<PostDocument> p = repository.findByPostTypeAndSubCategoryAndPostStatus(PostDocument.PICTURE, categoryId, "1", pageRequest);
		LOGGER.debug("Exit :: ExplorePictures()");
		return p;
	}
	
	@Override
	public Page<PostDocument> ExploreVideos(String categoryId, Pageable pageable) {
		LOGGER.debug("Enter in ExploreVideos()");
		PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(new Order(Direction.DESC, PostDocument.CREATION_DATE)));
		LOGGER.info("getPageNumber : "+pageRequest.getPageNumber()+" offset"+pageRequest.getOffset()+" pagesize"+pageRequest.getPageSize());
		Page<PostDocument> p = repository.findByPostTypeAndSubCategoryAndPostStatus(PostDocument.VIDEO, categoryId, "1", pageRequest);
		LOGGER.debug("Exit :: ExploreVideos()");
		return p;
	}

	@Override
	public Page<PostDocument> searchFourPostsByPostType(String postType, SearchQueries searchQueries, String timeZone) {
		StringBuilder keywords = new StringBuilder();
		List<String> searchList = Arrays.asList(searchQueries.getSearchText().split("\\s+"));
		for (Iterator<String> iterator = searchList.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			keywords.append(PostDocument.SELECT_ALL + string + PostDocument.SELECT_ALL + " ");
		}
		ArrayList<Order> orderList = new ArrayList<Sort.Order>();
		Order order1 = new Order(Direction.DESC, PostDocument.CREATION_DATE);
		Order order2 = new Order(Direction.DESC, PostDocument.RATING);
		orderList.add(order2);
		orderList.add(order1);
		PageRequest pageRequest =  new PageRequest(0, 4, new Sort(orderList));
		//PageRequest pageRequest =  new PageRequest(0, 4, new Sort(new Order(Direction.DESC, PostDocument.RATING)));
		String startRate, endRate;
		String strrate = searchQueries.getRating();
		long rate;
		
		try{
			rate = Long.parseLong(strrate);
			startRate = ""+(rate);
			endRate = PostDocument.MAX_RATE;
			
		}catch(Exception e){
			startRate = PostDocument.SELECT_ALL;
			endRate = PostDocument.SELECT_ALL;
		}
		String startPrice, endPrice;
		
		try{
			startPrice = ""+Long.parseLong(searchQueries.getStartPrice());
			
		}catch(Exception e){
			startPrice =  PostDocument.SELECT_ALL;
		}
		
		try{
			
			endPrice = ""+Long.parseLong(searchQueries.getEndPrice());
		}catch(Exception e){
			endPrice = PostDocument.SELECT_ALL;
		}
		
		LOGGER.debug("searchQueries.getStartPrice() : {} , searchQueries.endPrice : {} , start rate : {} , end rate : {}, queriesRate ; {} ",
				startPrice, endPrice, startRate, endRate, searchQueries.getRating());
		String pt = "*,*";
		Page<PostDocument> results ;
		if( postType.equalsIgnoreCase("recipe") && 
				(searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))){
			
				results = repository.searchPostsByPostType( keywords.toString(), postType, 
						"*", "*", startRate, endRate, pageRequest);

		}
		else if(searchQueries.getDistance() != null   && httpSession.getAttribute(Constants.IS_LOCATION_FOUND) != null && 
			!httpSession.getAttribute(Constants.IS_LOCATION_FOUND).equals("false") &&
			(postType.equalsIgnoreCase("dish") || postType.equalsIgnoreCase("product")) ){
			Date currentDate = HelpingClass.getTodayDateWithoutTimeInDateObj(timeZone);
			pt = (String) httpSession.getAttribute("userLat");
			pt = pt + "," + (String) httpSession.getAttribute("userLong");
			Point point = new Point(Double.parseDouble((String) httpSession.getAttribute("userLat")),
					Double.parseDouble((String) httpSession.getAttribute("userLong")));
			
			Distance distance;
			if(searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase("")){
				distance = PostDocument.MAX_DISTANCE;
			}
			else{
				Double dist = Double.parseDouble(searchQueries.getDistance());
				if(((String)httpSession.getAttribute("country_code")).equalsIgnoreCase("US")){
					dist = dist * 1.6d;
				}
				distance = new Distance(dist);
			}
			String countryCode = "*";
			if(httpSession.getAttribute("country_code")!=null)
				countryCode = (String) httpSession.getAttribute("country_code");
			results = repository.searchPostsByPostType( keywords.toString(), postType, 
					startPrice, endPrice, startRate, endRate, point, distance, countryCode, currentDate, pageRequest);
		}
		else if( postType.equalsIgnoreCase("picture") || postType.equalsIgnoreCase("video") ){

			results = repository.searchPostsByPostType( keywords.toString(), postType, 
					"*", "*", pageRequest);
		}
		else if( postType.equalsIgnoreCase("person") ){

			results = repository.searchPersons( keywords.toString(), postType, 
					startRate, endRate, pageRequest);
		}
		else{
			results = repository.searchPostsByPostType( keywords.toString(), postType, 
					startPrice, endPrice, startRate, endRate, pageRequest);
		}
		LOGGER.debug("--------------------------------------------------"
				+ "\n \n findAllFilterPostTypeContainsDesrciption postname result.size {}, postType: {}, searchText:{}", 
				results.getContent().size(), postType, searchQueries.getSearchText());
		
		for(PostDocument result: results){
			LOGGER.debug("\nresult search result distance:{}, name: {} | des: {} | key : {} | type: {} | id: {}", 
					result.getScore(), result.getPostName(), result.getPostDescription(),
					result.getKeyPoints(), result.getPostType(), result.getPostId());
		}
		
		return results;
	}

	@Override
	public Page<PostDocument> viewMore(String searchText, String postType, Pageable pageRequest) {
		// TODO Auto-generated method stub
		
		
		return null;
	}

	@Override
	public Page<PostDocument> viewMoreInDept(SearchQueries searchQueries, String postType, String timeZone, Pageable pageable) {
		
		LOGGER.debug("Enter in viewMoreInDept() pageable.getPageNumber(): {}",pageable.getPageNumber());
		StringBuilder keywords = new StringBuilder();
		List<String> searchList = Arrays.asList(searchQueries.getSearchText().split("\\s+"));
		for (Iterator<String> iterator = searchList.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			keywords.append(PostDocument.SELECT_ALL + string + PostDocument.SELECT_ALL + " ");
		}
		ArrayList<Order> orderList = new ArrayList<Sort.Order>();
		Order order1 = new Order(Direction.DESC, PostDocument.CREATION_DATE);
		Order order2 = new Order(Direction.DESC, PostDocument.RATING);
		orderList.add(order2);
		orderList.add(order1);
		PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(orderList));
		//PageRequest pageRequest =  new PageRequest(pageable.getPageNumber(), 12, new Sort(new Order(Direction.DESC, PostDocument.RATING)));
//		Page<PostDocument> results = repository.searchPostsByPostType(keywords.toString(), postType, pageRequest);
		String startRate, endRate;
		String strrate = searchQueries.getRating();
		long rate;
		try{
			rate = Long.parseLong(strrate);
			startRate = ""+(rate);
			endRate = PostDocument.MAX_RATE;
			
		}catch(Exception e){
			startRate = PostDocument.SELECT_ALL;
			endRate = PostDocument.SELECT_ALL;
		}
		String startPrice, endPrice;
		
		try{
			startPrice = ""+Long.parseLong(searchQueries.getStartPrice());
			
		}catch(Exception e){
			startPrice =  PostDocument.SELECT_ALL;
		}
		
		try{
			
			endPrice = ""+Long.parseLong(searchQueries.getEndPrice());
		}catch(Exception e){
			endPrice = PostDocument.SELECT_ALL;
		}
		String latitude, longitude;
		latitude = searchQueries.getLatitude();
		longitude = searchQueries.getLongitude();
		if( (latitude == null || latitude.equals("")) || (longitude == null || longitude.equals(""))){
			latitude = (String) httpSession.getAttribute("userLat");
			longitude = (String) httpSession.getAttribute("userLong");
		}
		Page<PostDocument> results ;
		if(postType.equalsIgnoreCase("recipe") && 
				(searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))){
			
				results = repository.searchPostsByPostType( keywords.toString(), postType, 
						startPrice, endPrice, startRate, endRate, pageRequest);

		}
		else if(searchQueries.getDistance() != null   && httpSession.getAttribute(Constants.IS_LOCATION_FOUND) != null && 
				!httpSession.getAttribute(Constants.IS_LOCATION_FOUND).equals("false") &&
				(postType.equalsIgnoreCase("dish") || postType.equalsIgnoreCase("product"))){
			Date currentDate = HelpingClass.getTodayDateWithoutTimeInDateObj(timeZone);
			Point point = new Point(Double.parseDouble(latitude),
					Double.parseDouble(longitude));
			Distance distance;
			if(searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase("")){
				distance = PostDocument.MAX_DISTANCE;
			}
			else{
				Double dist = Double.parseDouble(searchQueries.getDistance());
				if(((String)httpSession.getAttribute("country_code")).equalsIgnoreCase("US")){
					dist = dist * 1.6d;
				}
				distance = new Distance(dist);
			}
			String countryCode = "*";
			if(httpSession.getAttribute("country_code")!=null)
				countryCode = (String) httpSession.getAttribute("country_code");
			results = repository.searchPostsByPostType( keywords.toString(), postType, 
					startPrice, endPrice, startRate, endRate, point, distance, countryCode, currentDate, pageRequest);
		}
		else if( postType.equalsIgnoreCase("picture") || postType.equalsIgnoreCase("video") ){

			results = repository.searchPostsByPostType( keywords.toString(), postType, 
					"*", "*", pageRequest);
		}
		else if( postType.equalsIgnoreCase("person") ){

			results = repository.searchPersons( keywords.toString(), postType, 
					startRate, endRate, pageRequest);
		}
		else{

			results = repository.searchPostsByPostType( keywords.toString(), postType, 
					startPrice, endPrice, startRate, endRate, pageRequest);
		}
		LOGGER.debug("--------------------------------------------------"
				+ "\n \n findAllFilterPostTypeContainsDesrciption postname result.size {}, postType: {}, searchText:{}", 
				results.getContent().size(), postType, searchQueries.getSearchText());
		/*for(PostDocument result: results){
			LOGGER.debug("\nresult search result location:{}, name: {} | des: {} | key : {} | type: {} | id: {}", 
					result.getScore(), result.getPostName(), result.getPostDescription(),
					result.getKeyPoints(), result.getPostType(), result.getPostId());
		}*/
		return results;
	}

	@Override
	public void updateNumberOfRatingsAndAverageRating(String postUniqueId, long longValue, float averageRating) {
		repository.updateNumOfRatingsAndAverageRating(postUniqueId, longValue, averageRating);
		
	}

	@Override
	public void updateInIndex(PostDocument postDocument) {

		LOGGER.debug("updating document with information: {}", postDocument);
		this.addToIndex(postDocument);
//		repository.updatePostDocument(postDocument);
		
	}

	@Override
	public void updateAllPostsOfUser(List<PostDocument> listPostDocument) {
		repository.updateOwnerOfAllPosts(listPostDocument);
	}
	
	@Override
	public void updateAllStatusOfUser(List<PostDocument> listPostDocument) {
		repository.updateStatusOfAllPosts(listPostDocument);
	}

	@Override
	public List<PostDocument> getRelatedPosts(String postId, String postName,
			String postDescription, List<String> postKeyPoints, String postType,
			String countryCode, String timeZone) {
		PageRequest pageRequest =  new PageRequest(0, 5, new Sort(new Order(Direction.DESC, PostDocument.RATING)));
		if(postType.equalsIgnoreCase("recipe"))
			countryCode = "\"\"";
		StringBuilder postNameKeywords = new StringBuilder();
		List<String> postNameList = Arrays.asList(postName.split("\\s+"));
		for (Iterator<String> iterator = postNameList.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			postNameKeywords.append(PostDocument.SELECT_ALL + string + PostDocument.SELECT_ALL + " ");
		}
		
		StringBuilder postDescriptionKeywords = new StringBuilder();
		List<String> postDescriptionList = Arrays.asList(postDescription.split("\\s+"));
		for (Iterator<String> iterator = postDescriptionList.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			postDescriptionKeywords.append(PostDocument.SELECT_ALL + string + PostDocument.SELECT_ALL + " ");
		}
		Page<PostDocument> list;
		if(postType.equalsIgnoreCase("recipe"))
			list = repository.findRelatedPosts(postId, postDescriptionKeywords.toString(), postNameKeywords.toString(), postKeyPoints, postType, countryCode, pageRequest);
		else{
			Date currentDate = HelpingClass.getTodayDateWithoutTimeInDateObj(timeZone);
			list = repository.findRelatedPosts(postId, postDescriptionKeywords.toString(), postNameKeywords.toString(), postKeyPoints, postType, countryCode, currentDate, pageRequest);
		}
		/*for(PostDocument result: list){
			LOGGER.debug("\nfound related posts:{}, name: {} | des: {} | key : {} | type: {} | countrycode: {} "
					+ "| price: {}", 
					result.getPostName(), result.getPostDescription(), result.getKeyPoints().toString(),
					result.getKeyPoints(), result.getPostType(), result.getCountryCode(), result.getPrice());
		}*/
		return list.getContent();
	}
	
	@Override
	public void updateFollowFollowingUsers(List<PostDocument> listPostDocument){
		repository.updateDocumentsOfPersonTypeOnFollowFollowingAction(listPostDocument);
	}
	
	@Override
	public PostDocument findBySolrPostId(String postId){
		return repository.findByPostId(postId);
	}
}