package com.arc.chefya.jpa.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.arc.chefya.Pojo.Blog;

@Repository
public interface BlogJPARepository extends CrudRepository<Blog, Long>{

	@Query(value="select bl from Blog bl where bl.isVerified=1")
	public Page<Blog> findAll(Pageable pageable);
//	SELECT bs.BlogId FROM blog_subcategory as bs right join blog as b on bs.BlogId = b.BlogId where bs.SubCategoryId = 21 
	@Query(value="select b from BlogSubcategory bs right join bs.blog b where bs.subcategory.subCategoryId = :blog_subCatId and b.isVerified=1") 
	public Page<Blog>  findFilteredBlogsByCategoryId(@Param("blog_subCatId")Long blog_subCatId,Pageable pageable);
}
