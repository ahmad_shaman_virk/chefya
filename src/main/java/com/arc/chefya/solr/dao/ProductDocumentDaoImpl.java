package com.arc.chefya.solr.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.PartialUpdate;
import org.springframework.stereotype.Repository;

import com.arc.chefya.Pojo.Product;
import com.arc.chefya.document.PostDocument;

import javax.annotation.Resource;


@Repository
public class ProductDocumentDaoImpl implements PartialUpdateRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDocumentDaoImpl.class);

    @Resource
    private SolrTemplate solrTemplate;

    
    @Override
    public void update(Product productEntry) {
        LOGGER.debug("Performing partial update for product entry: {}", productEntry);

        
    }

	@Override
	public void updateNumOfLikes(String postUniqueId, String numberofLikes) {
		LOGGER.debug("\n updateNumberOfLikes unique key : {}, update likes: {}", postUniqueId, numberofLikes);
		PartialUpdate update = new PartialUpdate(PostDocument.POST_ID, postUniqueId);
		update.add(PostDocument.NUM_OF_LIKES, numberofLikes);
		solrTemplate.saveBean(update);
		solrTemplate.softCommit();
		
	}

	@Override
	public void updateNumOfRatingsAndAverageRating(String postUniqueId, long numberofratings, float averageRating) {
		LOGGER.debug("\n unique key : {}, update num ratings: {}, avg rating:{}", postUniqueId, numberofratings, averageRating);
		PartialUpdate update = new PartialUpdate(PostDocument.POST_ID, postUniqueId);
		update.add(PostDocument.NUM_OF_RATINGS, numberofratings);
		update.add(PostDocument.RATING, averageRating);
		solrTemplate.saveBean(update);
		solrTemplate.softCommit();
		
	}

	@Override
	public void updateOwnerOfAllPosts(List<PostDocument> listPostDocument) {
		List<PartialUpdate> listPartialUpdate = new ArrayList<PartialUpdate>();
		for (PostDocument postDocument : listPostDocument) {
			PartialUpdate update = new PartialUpdate(PostDocument.POST_ID, postDocument.getPostId());
			update.add(PostDocument.OWNER, postDocument.getOwnerID());
			listPartialUpdate.add(update);
		}
		solrTemplate.saveBeans(listPartialUpdate);
		solrTemplate.softCommit();
	}
	
	@Override
	public void updateStatusOfAllPosts(List<PostDocument> listPostDocument) {
		List<PartialUpdate> listPartialUpdate = new ArrayList<PartialUpdate>();
		for (PostDocument postDocument : listPostDocument) {
			PartialUpdate update = new PartialUpdate(PostDocument.POST_ID, postDocument.getPostId());
			update.add(PostDocument.POST_STATUS, postDocument.getPostStatus());
			listPartialUpdate.add(update);
		}
		solrTemplate.saveBeans(listPartialUpdate);
		solrTemplate.softCommit();
	}
	
	@Override
	public void setPostStatusHide(String postId){
		LOGGER.debug("\n setPostStatusHide id: {}", postId);
		PartialUpdate update = new PartialUpdate(PostDocument.POST_ID, postId);
		update.add(PostDocument.POST_STATUS, "0");
		solrTemplate.saveBean(update);
		solrTemplate.softCommit();
	}
	
	@Override
	public void updateDocumentsOfPersonTypeOnFollowFollowingAction(List<PostDocument> listPostDocument){
		LOGGER.debug("updateDocumentsOfPersonTypeOnFollowFollowingAction Start");
		List<PartialUpdate> listPartialUpdate = new ArrayList<PartialUpdate>();
		for (PostDocument postDocument : listPostDocument) {
			PartialUpdate update = new PartialUpdate(PostDocument.POST_ID, postDocument.getPostId());
			update.add(PostDocument.KEY_POINTS, postDocument.getKeyPoints());
			update.add(PostDocument.OWNER, postDocument.getOwnerID());
			LOGGER.debug("solr id: {}, keypoints:{}",postDocument.getPostId(), postDocument.getKeyPoints().toString());
			LOGGER.debug("solr id: {}, owner:{}",postDocument.getPostId(), postDocument.getOwnerID().toString());
			listPartialUpdate.add(update);
		}
		solrTemplate.saveBeans(listPartialUpdate);
		solrTemplate.softCommit();
	}
}