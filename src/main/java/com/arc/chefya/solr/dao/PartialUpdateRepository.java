package com.arc.chefya.solr.dao;

import java.util.List;

import com.arc.chefya.Pojo.Product;
import com.arc.chefya.document.PostDocument;

public interface PartialUpdateRepository {

    public void update(Product productEntry);
    
    public void updateNumOfLikes(String postUniqueId, String numberofLikes);
    
    public void updateNumOfRatingsAndAverageRating(String postUniqueId, long numberofratings, float averageRating);
    
    public void updateOwnerOfAllPosts(List<PostDocument> listPostDocument);

    public void setPostStatusHide(String postId);

    public void updateDocumentsOfPersonTypeOnFollowFollowingAction(List<PostDocument> listPostDocument);

	public void updateStatusOfAllPosts(List<PostDocument> listPostDocument);
}