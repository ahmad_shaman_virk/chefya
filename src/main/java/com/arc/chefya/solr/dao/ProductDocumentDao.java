package com.arc.chefya.solr.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Point;
import org.springframework.data.solr.core.geo.Distance;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.data.solr.repository.Facet;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;

import com.arc.chefya.document.PostDocument;

import java.util.Date;
import java.util.List;

public interface ProductDocumentDao extends PartialUpdateRepository, SolrCrudRepository<PostDocument, String> {

    
    
    public List<PostDocument> findByListLocationPointsWithin(Point p, Distance d);
    
    public List<PostDocument> findByListLocationPointsWithinAndPostType(Point p, Distance d, String postType);
    
	@Query(value = "{!geofilt pt=?0 sfield=locm_posts d=?1}", filters = { "postType:*?2*" })
	List<PostDocument> findAllFilterPostTypeAndListLocationPointsWithin(Point pt, Distance d, String postType);
    
	@Query(value = "postDescription:(?0) OR postName:(?0)", filters = { "postType:(?1) " })
	public List<PostDocument> findAllFilterPostTypeContains(String searchText, List<String> postType);
	
	// This query is used for products and dishes
	@Query(fields = { "*", "score"}, value = "{!geofilt score=distance sfield=locm_posts pt=?6 d=?7}", 
			 filters = { "postType:(?1) AND (postDescription:(?0) OR postName:(?0) OR keyPoints:(?0) ) "
			 		+ "AND price:[?2 TO ?3] AND rating:[?4 TO ?5] AND countryCode:(?8) AND postStatus:(1) "
			 		+ "AND endAvailabilityDate:[?9 TO *] AND startAvailabilityDate:[* TO ?9]" })
	public Page<PostDocument> searchPostsByPostType(String searchText, String postType, String startPrice, String endPrice, 
			String startRate, String endRate, Point pt, Distance d, String countryCode, Date currentDate, Pageable pageable);
	
	
	
	
	// This query is for recipes
	@Query(value = "( postName:(?0) OR keyPoints:(?0) OR postDescription:(?0) OR ingredients:(?0)) AND price:[?2 TO ?3] AND "
			+ "rating:[?4 TO ?5]  AND postStatus:(1)", filters = { "postType:(?1) " })
	public Page<PostDocument> searchPostsByPostType(String searchText, String postType, String startPrice, String endPrice, 
			String startRate, String endRate, Pageable p);
	
	// This query is for videos, pictures, persons
	@Query(value = "( postName:(?0) ) AND "
			+ "rating:[?2 TO ?3] AND postStatus:(1)", filters = { "postType:(?1) " })
	public Page<PostDocument> searchPostsByPostType(String searchText, String postType,  
				String startRate, String endRate, Pageable p);
	
	// This query is for videos, pictures, persons
		@Query(value = "( postName:(?0) ) AND "
				+ "rating:[?2 TO ?3]", filters = { "postType:(?1) " })
		public Page<PostDocument> searchPersons(String searchText, String postType,  
					String startRate, String endRate, Pageable p);
		
	public Page<PostDocument> findByPostTypeAndSubCategory(String postType,String categoryId, Pageable pageable);
	
	public Page<PostDocument> findByPostTypeAndSubCategoryAndPostStatus(String postType, String categoryId, String postStatus, Pageable pageable);
	
	// This query is for explore products and dishes
	@Query(fields = { "*", "score"}, value = "{!geofilt score=distance sfield=locm_posts pt=?2 d=?3}", 
			 filters = { "postType:(?0) AND subCategory:(?1) AND countryCode:(?4) AND postStatus:(1) "
			 		+ "AND endAvailabilityDate:[?5 TO *] AND startAvailabilityDate:[* TO ?5]" })
	public Page<PostDocument> findByPostTypeAndSubCategoryWithLocation(String postType,String categoryId, Point pt, Distance d,
			String countryCode, Date currentDate, Pageable pageable);

	@Query(value = "( postName:(?0) OR keyPoints:(?0) OR postDescription:(?0) ) AND postStatus:(1)", filters = { "postType:(?1) " })
	public Page<PostDocument> searchPostsByPostType(String searchText, String postType, Pageable p);
	
	@Query(value = "( postName:(?2) OR keyPoints:(?3) OR postDescription:(?1) ) AND -postId:\"?0\" AND postStatus:(1) "
			+ "AND endAvailabilityDate:[?6 TO *] AND startAvailabilityDate:[* TO ?6]", filters = { "postType:(?4) AND countryCode:(?5)" })
	public Page<PostDocument> findRelatedPosts(String postId, String postDescription, String postName, List<String> keyPoints, String postType, 
			String countryCode, Date currentDate, Pageable p);
	
	@Query(value = "( postName:(?2) OR keyPoints:(?3) OR postDescription:(?1) ) AND -postId:\"?0\" AND postStatus:(1)", filters = { "postType:(?4) AND countryCode:(?5)" })
	public Page<PostDocument> findRelatedPosts(String postId, String postDescription, String postName, List<String> keyPoints, String postType, String countryCode, Pageable p);
	
	public PostDocument findByPostId(String postId);
}