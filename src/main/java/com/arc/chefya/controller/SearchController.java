package com.arc.chefya.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.Point;
import org.springframework.data.solr.core.geo.Distance;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.arc.chefya.cart.CartStatusIndicator;
import com.arc.chefya.cart.ICartHandler;
import com.arc.chefya.document.PostDocument;
import com.arc.chefya.forms.SearchQueries;
import com.arc.chefya.forms.SearchRelatedPostQuery;
import com.arc.chefya.forms.SearchRelatedPostResponse;
import com.arc.chefya.forms.TempSearch;
import com.arc.chefya.forms.UserPostForm;
import com.arc.chefya.mapper.SearchResultMapper;
import com.arc.chefya.service.PostIndexService;
import com.arc.chefya.util.HelpingClass;
import com.arc.chefya.util.PageWrapper;

/**
 * This cleass serves requests of search both for mobile and web clients   
 **/
@Controller
public class SearchController {

	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);

	@Autowired
	PostIndexService postIndexService;
	@Autowired 
	ICartHandler handler;
	CartStatusIndicator cartIndicator;
	@Autowired
	HttpSession httpSession;
	
	/**
	 * This function returns the search page with some model objects. This will only return the page, not the search posts
	 * @param locale Locale local of the the client
	 * @param searchText String text for search
	 * @param request HttpServletRequest
	 * @return modelAndView contains: searchPage text, SearchQueries set with default values and provided search text and some others required model objects 
	 * */
	public ModelAndView searchBy(Locale locale, String searchText, HttpServletRequest request) {
		logger.debug("Enter in gotoSearchPage()");
		String query = "";
		try {
			query = URLDecoder.decode(request.getQueryString() , "utf-8");
		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
		} catch (Exception e) {

		}
		
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();


		for (int i = 0; i < params.length; i++) {
			String[] splitedParam = params[i].split("=");
			if(splitedParam.length>1)
				map.put(splitedParam[0], splitedParam[1]);
			else {
				logger.debug("no keyword");
				//return result;
			}
		}
		String isProduct 	= map.get("isProduct");		
		String isPicture 	= map.get("isPicture");
		String isDish 	 	= map.get("isDish");
		String isRecipe  	= map.get("isRecipe");
		String isVideo		= map.get("isVideo");
		String isPerson		= map.get("isPerson");
		//String searchText 	= map.get("searchText");
		logger.debug("\n \n Enter in gotoglobalsearch()"+searchText + " A: " +isPicture+ " R: " + isRecipe + 
				"  D: " + isDish + " P: " + isProduct + "Vid : " + isVideo + " Per : " + isPerson);
		ArrayList<String> postType = new ArrayList<String>();
		SearchQueries searchQueries = new SearchQueries();
		// TODO : validation on search parameters
		if(isDish 	 != null ){
			postType.add("dish");
			searchQueries.setIsDish("true");
		}

		if(isPicture != null){
			postType.add("picture");
			searchQueries.setIsPicture("true");
		}

		if(isProduct != null){
			postType.add("product");
			searchQueries.setIsProduct("true");
		}

		if(isRecipe  != null){
			postType.add("recipe");
			searchQueries.setIsRecipe("true");
		}
		
		if(isVideo  != null){
			postType.add("video");
			searchQueries.setIsVideo("true");
		}
		
		if(isPerson  != null){
			postType.add("person");
			searchQueries.setIsPerson("true");
		}
		
		if(isDish == null && isPicture == null && isProduct == null && isRecipe  == null && isVideo == null && isPerson == null){
			searchQueries.setIsDish("true");
			searchQueries.setIsPicture("true");
			searchQueries.setIsProduct("true");
			searchQueries.setIsRecipe("true");
			searchQueries.setIsVideo("true");
			searchQueries.setIsPerson("true");
		}
		
		if(searchText == null || searchText.equals("") ||  searchText.equals("null"))
			searchText = "*";
		
		searchQueries.setSearchText(searchText);
		ModelAndView modelAndView = new ModelAndView("searchpage");
		modelAndView.addObject("searchQueries", searchQueries);
		modelAndView.addObject("loggedinlocation", httpSession.getAttribute("loggedinlocation"));
		modelAndView.addObject("country_code", httpSession.getAttribute("country_code"));
		cartIndicator = new CartStatusIndicator();
		modelAndView=cartIndicator.displayCart(handler, logger, modelAndView);
		
		/
		modelAndView.addObject("currentLocale",locale.getLanguage());
		return modelAndView;
	}
	
	/**
	 * This function fulfil the request of search page with some search text
	 * @return ModelAndView received by searchby() function 
	 * @see this.searchby()
	 * */
	@RequestMapping(value = "/searchby/{searchText}", method = RequestMethod.GET)
	public ModelAndView gotoSearchPage(Locale locale, @PathVariable String searchText, HttpServletRequest request) {
		return searchBy(locale, searchText, request);
	}
	
	/**
	 * This function fulfil the request of search page without some search text
	 * @return ModelAndView received by searchby() function 
	 * @see this.searchby()
	 * */
	@RequestMapping(value = "/searchby", method = RequestMethod.GET)
	public ModelAndView gotoSearchPage(Locale locale, HttpServletRequest request) {
		return searchBy(locale, "*", request);
	}

	/**
	 * This function returns the search page with some model objects
	 * @deprecated
	 * @return modelAndView contains: searchPage text, SearchQueries set with default values and provided search text and some others required model objects
	 * 
	 */
	@RequestMapping(value = "/globalsearch", method = RequestMethod.POST)
	public ModelAndView gotoglobalsearch(@RequestBody String query) {

		try {
			query = URLDecoder.decode(query, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();


		for (int i = 0; i < params.length; i++) {
			String[] splitedParam = params[i].split("=");
			if(splitedParam.length>1)
				map.put(splitedParam[0], splitedParam[1]);
			else {
				logger.debug("no keyword");
				//return result;
			}
		}
		String isProduct 	= map.get("isProduct");		
		String isPicture 	= map.get("isPicture");
		String isDish 	 	= map.get("isDish");
		String isRecipe  	= map.get("isRecipe");
		String isVideo		= map.get("isVideo");
		String isPerson		= map.get("isPerson");
		String searchText 	= map.get("searchText");
		logger.debug("\n \n Enter in gotoglobalsearch()"+searchText + " A: " +isPicture+ " R: " + isRecipe + 
				"  D: " + isDish + " P: " + isProduct + "Vid : " + isVideo + " Per : " + isPerson);
		ArrayList<String> postType = new ArrayList<String>();
		SearchQueries searchQueries = new SearchQueries();
		// TODO : validation on search parameters
		if(isDish 	 != null ){
			postType.add("dish");
			searchQueries.setIsDish("true");
		}

		if(isPicture != null){
			postType.add("picture");
			searchQueries.setIsPicture("true");
		}

		if(isProduct != null){
			postType.add("product");
			searchQueries.setIsProduct("true");
		}

		if(isRecipe  != null){
			postType.add("recipe");
			searchQueries.setIsRecipe("true");
		}
		
		if(isVideo  != null){
			postType.add("video");
			searchQueries.setIsVideo("true");
		}
		
		if(isPerson  != null){
			postType.add("person");
			searchQueries.setIsPerson("true");
		}
		
		if(isDish == null && isPicture == null && isProduct == null && isRecipe  == null){
			searchQueries.setIsDish("true");
			searchQueries.setIsPicture("true");
			searchQueries.setIsProduct("true");
			searchQueries.setIsRecipe("true");
			searchQueries.setIsVideo("true");
			searchQueries.setIsPerson("true");
		}
		
		if(searchText == null || searchText.equals("") ||  searchText.equals("null"))
			searchText = "*";
		
		searchQueries.setSearchText(searchText);
//		List<PostDocument> result = iPostIndexService.globalSearch(searchText, postType);
		
//		iPostIndexService.findTop2ByPostType(postType.get(0), searchText);
		
		
		ModelAndView modelAndView = new ModelAndView("searchpage");
		modelAndView.addObject("searchQueries", searchQueries);
		modelAndView.addObject("loggedinlocation", httpSession.getAttribute("loggedinlocation"));
		modelAndView.addObject("country_code", httpSession.getAttribute("country_code"));
		cartIndicator = new CartStatusIndicator();
		modelAndView=cartIndicator.displayCart(handler, logger, modelAndView);
		return modelAndView;
	}

	/**
	 * This function returns the actual search result. Its is used to return first search page where posts are shown in groups.
	 * This url controller is build for mobile clients
	 * @return ModelAndView contains the result posts and updated search queries updated method
	 * */
	@ResponseBody
	@RequestMapping(value = "/app/globalSearchFourPostsByType", method = RequestMethod.POST)
	public HashMap<String, List<UserPostForm>> getFourPostsbyPostsTypeForMobile(@RequestBody SearchQueries searchQueries, HttpSession session){
		
		return searchFourPostsInCateogries(searchQueries,session);
	}
	
	/**
	 * This function returns the actual search result. Its is used to return first search page where posts are shown in groups.
	 * This url controller is build for web clients and it is called in AJAX requests to fecth search results.
	 * @return ModelAndView contains the result posts and updated search queries updated method
	 * */
	@ResponseBody
	@RequestMapping(value = "/globalSearchForPostsByType", method = RequestMethod.GET)
	public HashMap<String, List<UserPostForm>> getFourPostsbyPostsType(@ModelAttribute SearchQueries searchQueries, HttpSession session){
		
		return searchFourPostsInCateogries(searchQueries,session);
	}
	
	/**
	 * @deprecated
	 * Function used for testing purpose
	 * */
	@RequestMapping("/tempsearch2")
	public ModelAndView tempsearch2(TempSearch tt) {
		logger.debug("Enter in tempsearch2 "+tt.getSearchText()+" type :"+tt.getPostType()+"***");

		Point point = new Point( Double.parseDouble(tt.getLat()), Double.parseDouble(tt.getLongi()));
		Distance distance = new Distance( Double.parseDouble(tt.getDis()));

		if(tt.getPostType()!=null && tt.getPostType() != ""){
			postIndexService.searchWithinDistanceAndType(point, distance, tt.getSearchText(), tt.getPostType());
		}

		TempSearch t = new TempSearch();
		ModelAndView mv = new ModelAndView("tempsearch");
		mv.addObject("searchObj", t);
		return mv;
	}

	/**
	 * This function returns the view more search page. This will only return the page, not the search posts
	 * Once the page on web client is rendered then the URI of viewMoreInDeptLoadMore will be called in AJAX request to fetch data.
	 * @param deptName String used to show that in which dept the view more search page is requested
	 * @param request HttpServletRequest used to get request parameters of search filters
	 * @return modelAndView returns the search page and some model objects including searchqueries
	 * */
	@RequestMapping(value = "/search/{deptName}", method = RequestMethod.GET)
	 public ModelAndView viewMoreInDept(Locale locale, @PathVariable String deptName,
			 HttpServletRequest request, HttpSession session) {
		
		String query = "";
		try {
			query = URLDecoder.decode(request.getQueryString() , "utf-8");
		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
		} catch(Exception e){
			
		}
		
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();


		for (int i = 0; i < params.length; i++) {
			String[] splitedParam = params[i].split("=");
			if(splitedParam.length>1)
				map.put(splitedParam[0], splitedParam[1]);
			else {
				logger.debug("no keyword");
				//return result;
			}
		}
		
		String item = map.get("item");
		String startPrice = map.get("startPrice");
		String endPrice = map.get("endPrice");
		String location = map.get("location");
		String rating = map.get("rating");
		String searchQuery = map.get("searchQuery");
		logger.debug("\n viewMoreinDept : {} ; item : {} , startPrice : {} , endPrice : {} , location : {} , rating : {}",
				deptName,item,startPrice,endPrice,location,rating);
		
		ModelAndView modelAndView = new ModelAndView("viewmoresearchpage");
		
		cartIndicator = new CartStatusIndicator();
		modelAndView=cartIndicator.displayCart(handler, logger, modelAndView);
		modelAndView.addObject("currentLocale",locale.getLanguage());
			
		SearchQueries searchQueries = new SearchQueries();
		if(deptName != null && !query.equalsIgnoreCase("")){
			searchQueries.setStartPrice(startPrice);
			searchQueries.setEndPrice(endPrice);
			searchQueries.setRating(rating);
			searchQueries.setSearchText(searchQuery);
			searchQueries.setLatitude((String) httpSession.getAttribute("userLat"));
			searchQueries.setLongitude((String) httpSession.getAttribute("userLong"));
			
			if(deptName.equalsIgnoreCase("dish")){
				searchQueries.setViewMoreSearchIn(deptName);
				
				modelAndView.addObject("searchQueries", searchQueries);
				return modelAndView;
			}
			if(deptName.equalsIgnoreCase("product")){
				searchQueries.setViewMoreSearchIn(deptName);
				
				modelAndView.addObject("searchQueries", searchQueries);
				return modelAndView;
			}
			if(deptName.equalsIgnoreCase("recipe")){
				searchQueries.setViewMoreSearchIn(deptName);
				
				modelAndView.addObject("searchQueries", searchQueries);
				return modelAndView;
			}
			if(deptName.equalsIgnoreCase("person")){
				searchQueries.setViewMoreSearchIn(deptName);
				
				modelAndView.addObject("searchQueries", searchQueries);
				return modelAndView;
			}
			if(deptName.equalsIgnoreCase("picture")){
				searchQueries.setViewMoreSearchIn(deptName);
				
				modelAndView.addObject("searchQueries", searchQueries);
				return modelAndView;
			}
			if(deptName.equalsIgnoreCase("video")){
				searchQueries.setViewMoreSearchIn(deptName);
				
				modelAndView.addObject("searchQueries", searchQueries);
				return modelAndView;
			}
			
		}
		else{
			searchQueries.setViewMoreSearchIn(deptName);
			
			modelAndView.addObject("searchQueries", searchQueries);
			return modelAndView;
		}
		return null;
	}
	
	/**
	 * This URI is used to call in AJAX requests from mobile clients to fetch search results provided the search text and filter values.
	 * @param pageable Pageable is used to provide the pagination functionality.
	 * @return HashMap<String, Object> conatins the search results and updated searchQueries object
	 * */
	@ResponseBody
	@RequestMapping(value = "/search/viewmore/{deptName}", method = RequestMethod.GET)
	 public HashMap<String, Object> viewMoreInDeptLoadMore(@PageableDefault Pageable pageable, @PathVariable String deptName, @RequestBody String query,
			 HttpServletRequest request, HttpSession session, Model model) {
		
		 String timeZone=  (String) session.getAttribute("timeZone");
		try {
			query = URLDecoder.decode(request.getQueryString() , "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();


		for (int i = 0; i < params.length; i++) {
			String[] splitedParam = params[i].split("=");
			if(splitedParam.length>1)
				map.put(splitedParam[0], splitedParam[1]);
			else {
				logger.debug("no keyword");
			}
		}
		
		String item = map.get("item");
		String startPrice = map.get("startPrice");
		String endPrice = map.get("endPrice");
		String location = map.get("location");
		String latitude = map.get("mapLat");
		String longitude = map.get("mapLong");
		if(location == null)
			location = "";
		String rating = map.get("rating");
		String searchQuery = map.get("searchQuery");
		logger.debug("\n viewMoreInDeptLoadMore : {} ; item : {} , startPrice : {} , endPrice : {} , location : {} , rating : {}, lat:{}, longi:{}",
				deptName,item,startPrice,endPrice,location,rating,latitude,longitude);
		SearchQueries searchQueries = new SearchQueries();
		if(deptName != null){
			searchQueries.setStartPrice(startPrice);
			searchQueries.setEndPrice(endPrice);
			searchQueries.setRating(rating);
			searchQueries.setSearchText(searchQuery);
			searchQueries.setDistance(location);
			searchQueries.setLatitude(latitude);
			searchQueries.setLongitude(longitude);
			logger.debug("\n Current Page:{}",pageable.getPageNumber());
			if(deptName.equalsIgnoreCase("dish")){
				searchQueries.setViewMoreSearchIn(deptName);
				return viewMoreIndeptCont(deptName, searchQueries, session, pageable, timeZone);
				
			}
			else if(deptName.equalsIgnoreCase("product")){
				searchQueries.setViewMoreSearchIn(deptName);
				return viewMoreIndeptCont(deptName, searchQueries, session, pageable, timeZone);
				
			}
			/**
			 * Ignores the view more request if receive filter values of distance and prices other than default values
			 * */
			else if(deptName.equalsIgnoreCase("recipe")
					&& (searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))
					&& (searchQueries.getStartPrice().equalsIgnoreCase("") || searchQueries.getStartPrice().equalsIgnoreCase("ALL") || searchQueries.getStartPrice().equalsIgnoreCase("0"))
					&& (searchQueries.getEndPrice().equalsIgnoreCase("") || searchQueries.getEndPrice().equalsIgnoreCase("ALL")|| searchQueries.getEndPrice().equalsIgnoreCase("1000"))){
				searchQueries.setViewMoreSearchIn(deptName);
				return viewMoreIndeptCont(deptName, searchQueries, session, pageable, timeZone);
				
			}
			/**
			 * Ignores the view more request if receive filter values of distance and prices other than default values
			 * */
			else if(deptName.equalsIgnoreCase("person") && (searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))
					&& (searchQueries.getStartPrice().equalsIgnoreCase("") || searchQueries.getStartPrice().equalsIgnoreCase("ALL") || searchQueries.getStartPrice().equalsIgnoreCase("0"))
					&& (searchQueries.getEndPrice().equalsIgnoreCase("") || searchQueries.getEndPrice().equalsIgnoreCase("ALL")|| searchQueries.getEndPrice().equalsIgnoreCase("1000"))){
				searchQueries.setViewMoreSearchIn(deptName);
				return viewMoreIndeptCont(deptName, searchQueries, session, pageable, timeZone);
				
			}
			/**
			 * Ignores the view more request if receive filter values of rating, distance and prices other than default values
			 * */
			else if(deptName.equalsIgnoreCase("picture") 
					&& (searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))
					&& (searchQueries.getStartPrice().equalsIgnoreCase("") || searchQueries.getStartPrice().equalsIgnoreCase("ALL") || searchQueries.getStartPrice().equalsIgnoreCase("0"))
					&& (searchQueries.getEndPrice().equalsIgnoreCase("") || searchQueries.getEndPrice().equalsIgnoreCase("ALL")|| searchQueries.getEndPrice().equalsIgnoreCase("1000"))
					&& (searchQueries.getRating().equalsIgnoreCase("All") || searchQueries.getRating().equalsIgnoreCase(""))){
				searchQueries.setViewMoreSearchIn(deptName);
				return viewMoreIndeptCont(deptName, searchQueries, session, pageable, timeZone);
				
			}
			/**
			 * Ignores the view more request if receive filter values of rating, distance and prices other than default values
			 * */
			else if(deptName.equalsIgnoreCase("video") 
					&& (searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))
					&& (searchQueries.getStartPrice().equalsIgnoreCase("") || searchQueries.getStartPrice().equalsIgnoreCase("ALL") || searchQueries.getStartPrice().equalsIgnoreCase("0"))
					&& (searchQueries.getEndPrice().equalsIgnoreCase("") || searchQueries.getEndPrice().equalsIgnoreCase("ALL")|| searchQueries.getEndPrice().equalsIgnoreCase("1000"))
					&& (searchQueries.getRating().equalsIgnoreCase("All") || searchQueries.getRating().equalsIgnoreCase(""))){
				searchQueries.setViewMoreSearchIn(deptName);
				return viewMoreIndeptCont(deptName, searchQueries, session, pageable, timeZone);
				
			}
		}
		return null;
	}
	
	public HashMap<String, Object> viewMoreIndeptCont(String deptName, SearchQueries searchQueries, HttpSession session, Pageable pageable, String timeZone){
		Page<PostDocument> pagePostDoc = postIndexService.viewMoreInDept(searchQueries, deptName, HelpingClass.getUserTimeZone(session), pageable);
		
		PageWrapper<PostDocument> page = new PageWrapper<PostDocument>(pagePostDoc, "/search/viewmore/"+deptName);
		
		List<UserPostForm> postsForSearchPage = SearchResultMapper.getPostsForSearchPage(null, page.getContent(), null, (String) session.getAttribute("country_code"),timeZone);
		if(pagePostDoc.hasNext()){
			pagePostDoc.nextPageable().getPageNumber();page.getSize();
			logger.debug("\n pagePostDoc.nextPageable().getPageNumber(): {}, page.getSize() : {}",pagePostDoc.nextPageable().getPageNumber(),page.getSize());
			searchQueries.nextpage = "search/viewmore/"+deptName+"?page.page="+(pagePostDoc.nextPageable().getPageNumber()+1)+"&page.size="+page.getSize()+"item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
					"&location="+searchQueries.getDistance()+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText;
			logger.debug("\n viewMoreInDeptLoadMore , 	searchQueries.nextpage : {}",searchQueries.nextpage);	
		}
		logger.debug("\n\n\n pagePostDoc.getTotalPages() :"+pagePostDoc.getTotalPages());
		ArrayList<String> tempPageList = new ArrayList<String>();
		for (int i = 1; i <= pagePostDoc.getTotalPages(); i++) {
			String tempUrl = "search/viewmore/"+deptName+"?page.page="+i+"&page.size="+pagePostDoc.getSize()+"item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
					"&location="+searchQueries.getDistance()+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText;
			logger.debug("\n "+tempUrl);
			tempPageList.add(tempUrl);
		}
		logger.debug("\n Current Page:{}",pageable.getPageNumber());
		searchQueries.setCurrentPage(""+pageable.getPageNumber());
		searchQueries.setListPages(tempPageList);
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put(deptName, postsForSearchPage);
		hm.put("searchQueries", searchQueries);
		page.setUrl("viewmore/"+deptName+"?"+"item=all&startPrice="+searchQueries.startPrice+"&endPrice="+searchQueries.endPrice+
				"&location="+searchQueries.getDistance()+"&rating="+searchQueries.rating+"&searchQuery="+searchQueries.searchText);
		hm.put("page", page);
		return hm;
	}
	
	/**
	 * This function is used by URI of getFourPostsbyPostsType and getFourPostsbyPostsTypeForMobile.
	 * */
	public HashMap<String, List<UserPostForm>> searchFourPostsInCateogries(SearchQueries searchQueries, HttpSession session){
		HashMap<String, List<UserPostForm>> hashMap = new HashMap<String, List<UserPostForm>>();
		logger.debug("\n 111111111111 is search text: {}, is dish{}, IsPicture {}, IsProduct {}, IsRecipe {} , == {}",
				searchQueries.getSearchText(), searchQueries.getIsDish(), searchQueries.getIsPicture(),
				searchQueries.getIsProduct(), searchQueries.getIsRecipe(), searchQueries.toString());
		 String timeZone=  (String) session.getAttribute("timeZone");
		if((searchQueries.isDish == null || searchQueries.isDish.equals("")) 
				&& (searchQueries.isPicture == null || searchQueries.isPicture.equals(""))
				&& (searchQueries.isProduct == null || searchQueries.isProduct.equals(""))
				&& (searchQueries.isRecipe  == null || searchQueries.isRecipe.equals(""))
				&& (searchQueries.isVideo  == null || searchQueries.isVideo.equals(""))
				&& (searchQueries.isPerson  == null || searchQueries.isPerson.equals(""))){
			searchQueries.setIsDish("true");
			searchQueries.setIsPicture("true");
			searchQueries.setIsProduct("true");
			searchQueries.setIsRecipe("true");
			searchQueries.setIsVideo("true");
			searchQueries.setIsPerson("true");
		}
		
		if(searchQueries != null){
			logger.debug("\n is search text: {}, is dish{}, IsPicture {}, IsProduct {}, IsRecipe {} , == {}",
					searchQueries.getSearchText(), searchQueries.getIsDish(), searchQueries.getIsPicture(),
					searchQueries.getIsProduct(), searchQueries.getIsRecipe(), searchQueries.toString());
			
			if(searchQueries.getIsDish().equalsIgnoreCase("true")){
				
				logger.debug("\n --------------------------------- is dish: {}",searchQueries.getIsDish());
				Page<PostDocument> retval = postIndexService.searchFourPostsByPostType("dish", searchQueries, HelpingClass.getUserTimeZone(session));
				List<UserPostForm> postsForSearchPage = SearchResultMapper.getPostsForSearchPage(null, retval.getContent(), null, (String) session.getAttribute("country_code"),timeZone);
				logger.debug("\n --------------------------------- postsForSearchPage size: {}", postsForSearchPage.size());
				if(postsForSearchPage.size()>0)
					hashMap.put("dish", postsForSearchPage);
			}
			
			if(searchQueries.getIsPicture().equalsIgnoreCase("true")){
				
				logger.debug("\n ---------------------------------  IsPicture: {}",searchQueries.getIsPicture());
				/**
				 * Ignores the view more request if receive filter values of rating, distance and prices other than default values
				 * */
				if((searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))
						&& (searchQueries.getStartPrice().equalsIgnoreCase("") || searchQueries.getStartPrice().equalsIgnoreCase("ALL") || searchQueries.getStartPrice().equalsIgnoreCase("0"))
						&& (searchQueries.getEndPrice().equalsIgnoreCase("") || searchQueries.getEndPrice().equalsIgnoreCase("ALL")|| searchQueries.getEndPrice().equalsIgnoreCase("1000"))
						&& (searchQueries.getRating().equalsIgnoreCase("All") || searchQueries.getRating().equalsIgnoreCase(""))){
					Page<PostDocument> retval = postIndexService.searchFourPostsByPostType("picture", searchQueries,  HelpingClass.getUserTimeZone(session));
					List<UserPostForm> postsForSearchPage = SearchResultMapper.getPostsForSearchPage(null, retval.getContent(), null, (String) session.getAttribute("country_code"),timeZone);
					logger.debug("\n --------------------------------- postsForSearchPage size: {}", postsForSearchPage.size());
					if(postsForSearchPage.size()>0)
						hashMap.put("picture", postsForSearchPage);
				}
			}
			
			if(searchQueries.getIsProduct().equalsIgnoreCase("true")){
				
				logger.debug("\n ---------------------------------  IsProduct: {}",searchQueries.getIsProduct());
				Page<PostDocument> retval = postIndexService.searchFourPostsByPostType("product", searchQueries,  HelpingClass.getUserTimeZone(session));
				List<UserPostForm> postsForSearchPage = SearchResultMapper.getPostsForSearchPage(null, retval.getContent(), null, (String) session.getAttribute("country_code"),timeZone);
				logger.debug("\n --------------------------------- postsForSearchPage size: {}", postsForSearchPage.size());
				if(postsForSearchPage.size()>0)
					hashMap.put("product", postsForSearchPage);
			}
			
			if(searchQueries.getIsRecipe().equalsIgnoreCase("true")){
				
				logger.debug("\n ---------------------------------  IsRecipe: {}",searchQueries.getIsRecipe());
				/**
				 * Ignores the view more request if receive filter values of distance and prices other than default values
				 * */				
				if((searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))
						&& (searchQueries.getStartPrice().equalsIgnoreCase("") || searchQueries.getStartPrice().equalsIgnoreCase("ALL") || searchQueries.getStartPrice().equalsIgnoreCase("0"))
						&& (searchQueries.getEndPrice().equalsIgnoreCase("") || searchQueries.getEndPrice().equalsIgnoreCase("ALL")|| searchQueries.getEndPrice().equalsIgnoreCase("1000"))){
					Page<PostDocument> retval = postIndexService.searchFourPostsByPostType("recipe", searchQueries, HelpingClass.getUserTimeZone(session));
					List<UserPostForm> postsForSearchPage = SearchResultMapper.getPostsForSearchPage(null, retval.getContent(), null, (String) session.getAttribute("country_code"),timeZone);
					logger.debug("\n --------------------------------- postsForSearchPage size: {}", postsForSearchPage.size());
					if(postsForSearchPage.size()>0)
						hashMap.put("recipe", postsForSearchPage);
				}
			}
			
			if(searchQueries.getIsVideo().equalsIgnoreCase("true")){
				logger.debug("\n ---------------------------------  IsVideo: {}",searchQueries.getIsVideo());
				/**
				 * Ignores the view more request if receive filter values of rating, distance and prices other than default values
				 * */
				if((searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))
						&& (searchQueries.getStartPrice().equalsIgnoreCase("") || searchQueries.getStartPrice().equalsIgnoreCase("ALL") || searchQueries.getStartPrice().equalsIgnoreCase("0"))
						&& (searchQueries.getEndPrice().equalsIgnoreCase("") || searchQueries.getEndPrice().equalsIgnoreCase("ALL")|| searchQueries.getEndPrice().equalsIgnoreCase("1000"))
						&& (searchQueries.getRating().equalsIgnoreCase("All") || searchQueries.getRating().equalsIgnoreCase(""))){
					Page<PostDocument> retval = postIndexService.searchFourPostsByPostType("video", searchQueries,  HelpingClass.getUserTimeZone(session));
					List<UserPostForm> postsForSearchPage = SearchResultMapper.getPostsForSearchPage(null, retval.getContent(), null, (String) session.getAttribute("country_code"),timeZone);
					logger.debug("\n --------------------------------- postsForSearchPage size: {}", postsForSearchPage.size());
					if(postsForSearchPage.size()>0)
						hashMap.put("video", postsForSearchPage);
				}
			}
			
			if(searchQueries.getIsPerson().equalsIgnoreCase("true")){
				logger.debug("\n ---------------------------------  IsPerson: {}", searchQueries.getIsPerson());
				/**
				 * Ignores the view more request if receive filter values of distance and prices other than default values
				 * */
				if((searchQueries.getDistance().equalsIgnoreCase("ALL") || searchQueries.getDistance().equalsIgnoreCase(""))
						&& (searchQueries.getStartPrice().equalsIgnoreCase("") || searchQueries.getStartPrice().equalsIgnoreCase("ALL") || searchQueries.getStartPrice().equalsIgnoreCase("0"))
						&& (searchQueries.getEndPrice().equalsIgnoreCase("") || searchQueries.getEndPrice().equalsIgnoreCase("ALL")|| searchQueries.getEndPrice().equalsIgnoreCase("1000"))){
					Page<PostDocument> retval = postIndexService.searchFourPostsByPostType("person", searchQueries,  HelpingClass.getUserTimeZone(session));
					List<UserPostForm> postsForSearchPage = SearchResultMapper.getPostsForSearchPage(null, retval.getContent(), null, (String) session.getAttribute("country_code"),timeZone);
					logger.debug("\n --------------------------------- postsForSearchPage size: {}", postsForSearchPage.size());
					if(postsForSearchPage.size()>0)
						hashMap.put("person", postsForSearchPage);
				}
			}
		}
		return hashMap;
	}
	
	/**
	 * This URI is used to call in AJAX requests from mobile clients to fetch search results provided the search text and filter values.
	 * This function reuse the URI of viewMoreInDeptLoadMore and return the same object.
	 * @param pageable Pageable is used to provide the pagination functionality.
	 * @return HashMap<String, Object> conatins the search results and updated searchQueries object
	 * */
	@ResponseBody
	@RequestMapping(value = "search/search/viewmore/{deptName}", method = RequestMethod.GET)
	 public HashMap<String, Object> viewMoreInDeptLoadMoreTemp(@PageableDefault Pageable pageable, @PathVariable String deptName, @RequestBody String query,
			 HttpServletRequest request, HttpSession session, Model model) {
		
		logger.debug("viewMoreInDeptLoadMoreTemp");
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm = viewMoreInDeptLoadMore(pageable, deptName, query, request, session, model);
		
		logger.debug("viewMoreInDeptLoadMoreTemp hashmapp : "+hm.size()+"   sdf:"+hm.toString());
		return hm;
	}
	
	@ResponseBody
	@RequestMapping(value = "getRelatedPosts", method = RequestMethod.POST)
	public List<SearchRelatedPostResponse> getRelatedPosts(@RequestBody SearchRelatedPostQuery searchRelatedPost, HttpSession session){
		logger.debug("getRelatedPosts start");
		logger.debug("searchrelated post object : {}", searchRelatedPost.toString());
		List<PostDocument> list = postIndexService.getRelatedPosts(searchRelatedPost.getPostId(), searchRelatedPost.getPostName(), searchRelatedPost.getPostDescription()
				, searchRelatedPost.getKeyPoints(), searchRelatedPost.getPostType(), (String) httpSession.getAttribute("country_code"),  HelpingClass.getUserTimeZone(session));
		List<SearchRelatedPostResponse> retList = new ArrayList<SearchRelatedPostResponse>();
		for (PostDocument postDocument : list) {
			String newPrice ="";
			if(!postDocument.getPostType().equalsIgnoreCase("recipe"))
				newPrice = HelpingClass.getCurrencyWithSymbol(postDocument.getPrice()+"", postDocument.getCountryCode());

			retList.add(new SearchRelatedPostResponse(postDocument.getPostUrl(), postDocument.getPostName(),
					HelpingClass.averageRatingInPercentage(postDocument.getRating()), newPrice, postDocument.getPrimaryPicUrl()));
		}
		return retList;
	}
}
