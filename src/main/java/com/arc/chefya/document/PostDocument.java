package com.arc.chefya.document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.solr.client.solrj.beans.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;
import org.springframework.data.solr.core.geo.Distance;
import org.springframework.data.solr.core.mapping.Indexed;

import com.arc.chefya.controller.UserController;

public class PostDocument {

	private static final Logger logger = LoggerFactory.getLogger(PostDocument.class);
	
	public static final String POST_ID 			= "postId";
	public static final String POST_DESCRIPTION = "postDescription";
	public static final String POST_NAME 		= "postName";
	public static final String POST_TYPE 		= "postType";
	public static final String OWNER 			= "owner";
	public static final String SUBCATEGORY 		= "subCategory";
	public static final String PRICE 			= "price";
	public static final String CREATION_DATE	= "creationDate";
	public static final String KEY_POINTS 		= "keyPoints";
	public static final String PRIMARY_PIC_URL 	= "primaryPicUrl";
	public static final String POST_URL 		= "postUrl";
	public static final String POST_STATUS 		= "postStatus";
	public static final String NUM_OF_LIKES 	= "numOfLikes";
	public static final String NUM_OF_RATINGS 	= "numOfRatings";
	public static final String RATING 			= "rating";
	public static final String LOCATIONS 		= "locm_posts";
	public static final String SELECT_ALL		= "*";
	public static final String MAX_RATE			= "5";
	public static final Distance MAX_DISTANCE	= new Distance(5000);
	public static final String RECIPE			= "recipe";
	public static final String DISH				= "dish";
	public static final String PRODUCT			= "product";
	public static final String PICTURE			= "picture";
	public static final String VIDEO			= "video";
	public static final String COUNTRY_CODE		= "countryCode";
	
	@Id
	@Field
	private String postId;

	@Field
	private String postDescription;

	@Field
	private String postType;

	@Field
	private String postName;

	@Field
	private List<String> owner;

	@Field
	private List<String> subCategory;

	@Field
	private float price;

	@Field
	private Date  creationDate;

	@Field
//	private String keyPoints;
	private List<String> keyPoints;

	@Field
	private String primaryPicUrl;

	@Field
	private String postUrl;

	@Field
	private String postStatus;

	@Field
	private String numOfLikes = "0";

	@Field
	private long numOfRatings = 0;

	@Field
	private float rating ;
	
	@Field("locm_posts")
	private List<Point>  listLocationPoints;
	
	//distance field not to be written when adding document to solr
	@Indexed(readonly = true)
	private Float score;
	
	@Field
	private String countryCode;

	@Field
	private Date startAvailabilityDate;

	@Field
	private Date endAvailabilityDate;
	
	@Field
	private List<String> ingredients;

	private PostDocument() {

	}

	/*
	 *  Be aware of following:
	 *  1. Try not provide null value in functions of builder, These null values will save "null" as text in solr
	 *  e.g. if category parameter is provided null then null will be saved in Solr and can't be searched by query using
	 *  type of post
	 */
	public static Builder getBuilder(String id, String title) {
		return new Builder(id, title);
	}

	public String getId() {
		return postId;
	}

	/*
	 * At list.get(0) : user id will be returned as string  
	 * list.get(1) : user's username will be returned
	 * list.get(2) : user's first name and last name will be returned, , can be null if user has not provided this info
	 * list.get(3) : user's profile image will be returned, , can be null if user has not provided this info
	 * */
	public List<String> getOwnerID() {
		return owner;
	}

	/*
	 * At list.get(0) : subCategory id will be returned as string  
	 * list.get(1) : subCategory name will be returned
	 * */
	public List<String> getCategory() {
		return subCategory;
	}

	public float getPrice() {
		return price;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public List<String> getKeyPoints() {
		return keyPoints;
	}

	public String getPrimaryPicUrl() {
		return primaryPicUrl;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public String getPostStatus() {
		return postStatus;
	}

	public String getPostId() {
		return postId;
	}

	public String getPostDescription() {
		return postDescription;
	}

	public String getPostName() {
		return postName;
	}

	public String getNumOfLikes() {
		return numOfLikes;
	}

	public long getNumOfRatings() {
		return numOfRatings;
	}

	public float getRating() {
		return rating;
	}

	public String getPostType() {
		return postType;
	}
	
	public List<Point> getListLocationPoints() {
		return listLocationPoints;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public void setSubCategory(List<String> subCategory) {
		this.subCategory = subCategory;
	}

	/*
	 * Returning score as distance in miles
	 * */
	public String getScore() {
		if(score == null){
			return " N/A";
		}
		else {
			//logger.debug("posttype: {}, score: {}", this.getPostType(), score);
			return ""+ String.format("%.2f", (score*69.09341))+ " miles";
		}
//		return  (float) (score == null? score:score*111.1951);
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Date getStartAvailabilityDate() {
		return startAvailabilityDate;
	}

	public void setStartAvailabilityDate(Date startAvailabilityDate) {
		this.startAvailabilityDate = startAvailabilityDate;
	}

	public Date getEndAvailabilityDate() {
		return endAvailabilityDate;
	}

	public void setEndAvailabilityDate(Date endAvailabilityDate) {
		this.endAvailabilityDate = endAvailabilityDate;
	}

	public List<String> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<String> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public static class Builder {
		private PostDocument build;

		public Builder(String id, String productName) {
			build = new PostDocument();
			build.postId = id;
			build.postName = productName;
		}

		public Builder description(String productDescription) {
			build.postDescription = productDescription;
			return this;
		}

		public Builder postType(String postType){
			build.postType = postType;
			return this;
		}

		/**/
		public Builder ownerID(ArrayList<String> ownerID) {
			build.owner = ownerID;
			return this;
		}

		public Builder category(ArrayList<String> category) {
			build.subCategory = category;
			return this;
		}

		public Builder price(float f) {
			//if(price != null)
				build.price = f;
			return this;
		}

		public Builder creationDate(Date creationDate) {
			build.creationDate = creationDate;
			return this;
		}

		/**/
		public Builder keyPoints(ArrayList<String>  keyPoints) {
			build.keyPoints = keyPoints;
			return this;
		}

		public Builder primaryPicUrl(String primaryPicUrl) {
			build.primaryPicUrl = primaryPicUrl;
			return this;
		}

		public Builder postUrl(String postUrl) {
			build.postUrl = postUrl;
			return this;
		}

		public Builder postStatus(Integer postStatus) {
			if(postStatus != null)
				build.postStatus = postStatus.toString();
			return this;
		}

		public Builder numOfLikes(String numOfLikes){
			build.numOfLikes = numOfLikes;
			return this;
		}

		public Builder numOfRatings(long numOfRatings){
			//if(numOfRatings != null)
				build.numOfRatings = numOfRatings;
			return this;
		}

		public Builder rating(float rating){
//			if(rating != null)
				build.rating = rating;
			return this;
		}
		
		public Builder points(ArrayList<Point> list){
			build.listLocationPoints = list;
			return this;
		}
		
		public Builder countryCode(String countryCode){
			build.countryCode = countryCode;
			return this;
		}
		
		public Builder startAvailabilityDate(Date startAvailabilityDate){
			build.startAvailabilityDate = startAvailabilityDate;
			return this;
		}
		
		public Builder endAvailabilityDate(Date endAvailabilityDate){
			build.endAvailabilityDate = endAvailabilityDate;
			return this;
		}
		
		public PostDocument build() {
			return build;
		}
	}
}