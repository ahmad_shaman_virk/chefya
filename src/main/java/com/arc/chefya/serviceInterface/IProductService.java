package com.arc.chefya.serviceInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.json.simple.parser.ParseException;

import com.arc.chefya.Pojo.Dish;
import com.arc.chefya.Pojo.DishSubcategory;
import com.arc.chefya.Pojo.Product;
import com.arc.chefya.Pojo.ProductLike;
import com.arc.chefya.Pojo.ProductReported;
import com.arc.chefya.Pojo.ProductSubcategory;
import com.arc.chefya.Pojo.Recipe;
import com.arc.chefya.Pojo.RecipeSubcategory;
import com.arc.chefya.Pojo.Subcategory;
import com.arc.chefya.Pojo.User;
import com.arc.chefya.apiformdata.MobileShortItemDTO;
import com.arc.chefya.forms.HideContentForm;
import com.arc.chefya.forms.ItemCommentsForm;
import com.arc.chefya.forms.ProductForm;
import com.arc.chefya.forms.ItemForm;
import com.arc.chefya.forms.ProductShortForm;
import com.arc.chefya.forms.ReportContentForm;
import com.arc.chefya.model.Image;
import com.github.dandelion.datatables.core.ajax.DataSet;
import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
import com.notnoop.exceptions.InvalidSSLConfig;

public interface IProductService {

	Subcategory fetchAllProductsAgainstCategory(String department,
			String category);

	List<ProductSubcategory> fetchAllProductsAgainstCategory(String category);

	List<DishSubcategory> fetchAllDishesAgainstCategory(String category);

	public abstract List<RecipeSubcategory> fetchAllRecipesAgainstCategory(String category);

	public abstract Product fetchProductDetails(Long productId);

	public abstract Dish fetchDishDetails(Long dishId);

	public abstract Recipe fetchRecipeDetails(Long recipeId);
	
	public String GetUTCdatetimeAsString();
	
	public abstract ItemForm saveProduct(ProductForm form, String url);
	
	public abstract void saveProductImages(ProductForm form, String url);
	
	public abstract List<ProductShortForm> allProductsofACategory(java.lang.Long id);
	
	public abstract ProductForm productById(Long productId);
	
	public abstract HashMap likeUnlikeProduct(Long productId,User user,String status,String time) throws InvalidSSLConfig, IOException, ParseException;
	
	public abstract HashMap deleteComment(Long commentId);
	
	public abstract Long Commentproduct(Long productId,User user,String status,String time,String comment, String review,String taggedUsers) throws InvalidSSLConfig, IOException, ParseException;
	public abstract Long EditCommentproduct(Long productId,User user,String status,String time,String comment, String review,String taggedUsers,Long commentId) throws InvalidSSLConfig, IOException, ParseException;
	
	public abstract String countAverageRatings(Long productId);
	
	public abstract ArrayList<ItemCommentsForm> limitedProductComments(Long productId,Integer start, Integer total,String timeZone);
	public abstract boolean updateProduct(Product product); 
	public Product findByProductId(Long id);

	public abstract String reportContentofProduct(ReportContentForm reportContentForm);


	long getTotalLike(Long productId);


	List<ProductReported> fetchAllReportedProducts();

	void deleteReportedContent(String id,String reportId);

	void ignoreReportedContent(String id);

	public Long findByProductIdTotalRatings(String id);

	Long findByProductIdTotalComments(String productId);

	public abstract ItemForm editProduct(ProductForm form, String defaultImageUrl, ArrayList<Image> productsToBeDeleted);
	public ProductLike findByUserIdAndProductId(Long userId, Long productId);

	public String findTotalLikesofAProduct(Long productId);

	Long findByProductIdTotalRatings(Long productId);

	
	public abstract ArrayList<Product> findAllProductsByUserId(Long userId);

	void deleteProductById(Long productId);

	List<String> getRandomPicturesForLandingPage(HttpSession session, String timeZone2);

	public abstract DataSet findProductsofOwnerWithDatatablesCriterias(DatatablesCriterias criterias,Long ownerId,String timeZone);


	String hideContentofProduct(HideContentForm hideContentForm, String userId,
			String viaUserId);


	
	public void unpublishSharedProducts(long productId, int publishedStatus);

	ArrayList<Product> findAllProductsByUserIdandstatus(Long userId,
			String timeZone);
	

}