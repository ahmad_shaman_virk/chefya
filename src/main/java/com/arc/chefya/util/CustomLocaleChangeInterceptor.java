package com.arc.chefya.util;

import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.arc.chefya.Pojo.User;
import com.arc.chefya.serviceInterface.ICustomUserService;
import com.arc.chefya.serviceInterface.INotificationService;

public class CustomLocaleChangeInterceptor extends LocaleChangeInterceptor {

	//private static final Logger logger = LoggerFactory.getLogger(CustomLocaleChangeInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1,	Object arg2) throws ServletException {

		HttpSession session = arg0.getSession();
		ICustomUserService service = (ICustomUserService) RequestContextUtils.getWebApplicationContext(arg0).getBean("customUserService");
		INotificationService notificationService = (INotificationService) RequestContextUtils.getWebApplicationContext(arg0).getBean("notificationService");
		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		//String name = auth.getName();
		String userName = (String) session.getAttribute("userName");
		//logger.debug("User Name isssssssssssssss = " + userName);

		//logger.debug("Locale change is called and paramName: lang - newlocale= "+arg0.getParameter("lang"));

		if (arg0.getParameter("lang") == null) {
			//logger.debug("Enter in arg0.getParameter equal to null");

			try {				
				if (userName == null || userName.equalsIgnoreCase("anonymousUser")){
					//logger.debug("Enter in AnonymousUser Body");

				}else {
					//logger.debug("User name is : " + userName + "set current locale is : " + arg0.getParameter("lang"));					
					if (arg0.getParameter("lang") == null) {

					}else {
						/*User user = service.fetchUserDetail(userName);
						user.setLanguage(arg0.getParameter("lang"));
						service.updateUser(user);	*/
					}			
				}	

			} catch (Exception e) {
				e.printStackTrace();
			}
			session.setAttribute("currentLang", "en");

		}else {
			//logger.debug("Enter in arg0.getParameter not equal to null");

			String language = (String) session.getAttribute("currentLang");
			//logger.debug("Current language is already selected = "+ language);

			//logger.debug("current language is = " + language);
			//logger.debug("new language is = " + arg0.getParameter("lang"));

			if (userName == null || userName.equalsIgnoreCase("anonymousUser")){
				//logger.debug("Enter in AnonymousUser Body");

			}else {
				User user = service.fetchUserDetail(userName);
				user.setLanguage(arg0.getParameter("lang"));
				service.updateUser(user);		
				
				try {
					//logger.debug("Enter in Notification Service");
					notificationService.getUnreadNotificationDB(user.getUserId(),(String)session.getAttribute("timeZone"), session.getId());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}												
			session.setAttribute("currentLang", arg0.getParameter("lang"));

		}

		//logger.debug("Locale change is called");

		return super.preHandle(arg0, arg1, arg2);
	}
}
