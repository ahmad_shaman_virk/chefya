package com.arc.chefya.util;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class AjaxAwareAuthenticationEntryPoint 
             extends LoginUrlAuthenticationEntryPoint {
	private static final Logger logger = LoggerFactory
			.getLogger(	AjaxAwareAuthenticationEntryPoint.class );

    public AjaxAwareAuthenticationEntryPoint(String loginUrl) {
        super(loginUrl);
    }

    @Override
    public void commence(
        HttpServletRequest request, 
        HttpServletResponse response, 
        AuthenticationException authException) 
            throws IOException, ServletException {

    	String uri = request.getRequestURI();
    	
    	logger.debug("commence start Referer "+request.getHeader("Referer"));
        logger.debug("commence start referer "+request.getHeader("referer"));
        List<String> headerNames = Collections.list((Enumeration<String>)request.getHeaderNames());
        for (String headerName : headerNames) {
            logger.debug(headerName +" : "+request.getHeaders(headerName));
        }
    	
        boolean checkURL 
            = uri.contains("/user/");
        
        boolean ajaxRequest = false;
    
        Enumeration<String> ajaxHeaderValues = request
                .getHeaders("x-requested-with");
        
        while (ajaxHeaderValues.hasMoreElements()) {
			String header = (String) ajaxHeaderValues.nextElement();
			
			logger.debug("Found header: " + header);
			
			if (StringUtils.equalsIgnoreCase("XMLHttpRequest", header)) {
                ajaxRequest = true;
            }			
			
		}
        
        logger.debug("checkURL is : " + checkURL);

        logger.debug("ajaxRequest is : " + ajaxRequest);
        HttpSession session = request.getSession();
        if (ajaxRequest && checkURL) {
        	logger.debug("IFFFFFFFFFF commence end Referer "+request.getHeader("Referer"));
            logger.debug("IFFFFFFFFFF commence end referer "+request.getHeader("referer"));
            response.sendError(403, "Forbidden");
        } else {
        	if (uri != null) {
        		logger.debug("First URI is: " + uri);
            	
            	uri = uri.substring(uri.indexOf("/Chefya") + 7);
            	
            	logger.debug("second URI is: " + uri);
    		}
        	session.setAttribute("url_prior_login", uri);
            logger.debug("IFFFFFFFFFF URI is: " + uri);
            super.commence(request, response, authException);
        }
    }
    
    private boolean isAjaxRequest(HttpServletRequest request) {
        boolean isAjaxRequest = false;

        SavedRequest savedRequest = (SavedRequest) request.getSession()
                .getAttribute(
                		"SPRING_SECURITY_SAVED_REQUEST_KEY");
        
        logger.debug("savedRequest is : " + savedRequest);
        
        if (savedRequest != null) {
            List<String> ajaxHeaderValues = savedRequest
                    .getHeaderValues("x-requested-with");
            for (String value : ajaxHeaderValues) {
                if (StringUtils.equalsIgnoreCase("XMLHttpRequest", value)) {
                    isAjaxRequest = true;
                }
            }
        }
        return isAjaxRequest;
    }
    
    
}