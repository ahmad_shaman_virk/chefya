package com.arc.chefya.util;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler  {
	
	private Logger logger = Logger.getLogger(CustomAuthenticationFailureHandler.class);
	
	public CustomAuthenticationFailureHandler(){
		super();
	}
	

	@Override
	public void onAuthenticationFailure(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse, AuthenticationException authException)
			throws IOException, ServletException {
		
		
		logger.debug("######################### shammmmmmmmmmmmmmmmmmmmmmmm:" + authException.getMessage()+ " sham exception: "+authException.toString());
		String deviceType = httpServletRequest.getParameter("deviceType");
		logger.debug("deviceType:: " + deviceType);
		logger.debug(authException.getLocalizedMessage());
		logger.debug(authException.getClass());
		
		if(authException.toString().contains("org.springframework.security.authentication.LockedException")){
			logger.debug(" got and redirect to /login/userlock org.springframework.security.authentication.LockedException");
			RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("/login/userLock");
			dispatcher.forward(httpServletRequest, httpServletResponse); 
		}
		else if(authException.toString().contains("org.springframework.security.authentication.BadCredentialsException")){
			logger.debug(" got and redirect to /login/BadCredentials org.springframework.security.authentication.LockedException");
			RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("/login/BadCredentials");
			dispatcher.forward(httpServletRequest, httpServletResponse); 
		}
		else if(authException.toString().contains("org.springframework.security.core.userdetails.UsernameNotFoundException")){
			logger.debug(" got and redirect to /login/UsernameNotFound org.springframework.security.authentication.LockedException");
			RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("/login/UsernameNotFound");
			dispatcher.forward(httpServletRequest, httpServletResponse); 
		}
		else{
			RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("/login/failure");
			dispatcher.forward(httpServletRequest, httpServletResponse); 
		}
		

	}

}