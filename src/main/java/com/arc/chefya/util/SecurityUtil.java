package com.arc.chefya.util;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;

import com.arc.chefya.Pojo.User;
import com.arc.chefya.Pojo.UserRole;
import com.arc.chefya.forms.SpringUserDetails;

public class SecurityUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityUtil.class);
    
    

    public static void logInUser(HashMap hm, HttpServletRequest request, SessionRegistry sessionRegistry) {
    	User user = (User) hm.get("user");
    	LOGGER.info("Logging in user: {}", user);
        UserRole ur = (UserRole) hm.get("user_role");
        SpringUserDetails userDetails = SpringUserDetails.getBuilder()
        		.userContact(user.getUserContact())
        		.userEmail(user.getUserEmail())
        		.userName(user.getUserName())
        		.id(user.getUserId())
        		.userPassword(user.getUserPassword())
        		.role(ur.getRoleName())
        		.socialSignInProvider(user.getSocialMediaService())
        		.build();

        LOGGER.debug("Logging in principal: {}", userDetails);

        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);

        // Create a new session and add the security context.
        HttpSession session = request.getSession(true);
        session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
        /*for (Object obj : sessionRegistry.getAllPrincipals()) {
        	LOGGER.debug("SpringUserDetails: "+obj.toString());
        	LOGGER.debug("User name :"+((SpringUserDetails)obj).getUsername());
			
		}
        LOGGER.debug("session registry loop executed and registring now in session registry");*/
        sessionRegistry.registerNewSession(session.getId(), userDetails);
        
        /*LOGGER.debug("registered in session registry now executing the loop");
        for (Object obj : sessionRegistry.getAllPrincipals()) {
        	LOGGER.debug("SpringUserDetails: "+obj.toString());
        	LOGGER.debug("User name :"+((SpringUserDetails)obj).getUsername());
			
		}*/
        LOGGER.info("User: {} has been logged in.", userDetails);
    }
}