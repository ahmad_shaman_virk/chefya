package com.arc.chefya.util;

import javax.servlet.annotation.WebServlet;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

@WebServlet(description = "Http Servlet using pure java / annotations", urlPatterns = { "/mediaserver/*" }, name = "annotatedServletHandler")
public class AnnotatedHttpServlet extends HttpRequestHandlerServlet {
 
private static final long serialVersionUID = 1L;
}