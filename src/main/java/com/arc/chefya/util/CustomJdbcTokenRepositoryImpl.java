package com.arc.chefya.util;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.stereotype.Service;

import com.arc.chefya.controller.HomeController;
import com.arc.chefya.serviceInterface.ICustomUserService;

public class CustomJdbcTokenRepositoryImpl extends JdbcTokenRepositoryImpl{
	@Autowired
	ICustomUserService customUserService;
	
	
	/*private JdbcTemplate template;
	
	 private static SingleConnectionDataSource dataSource;
	 
	 private JdbcTokenRepositoryImpl repo;*/
	
	private static final Logger logger = LoggerFactory.getLogger(CustomJdbcTokenRepositoryImpl.class);
	
	
	/*@BeforeClass
    public static void createDataSource() {
        dataSource = new SingleConnectionDataSource("jdbc:hsqldb:mem:tokenrepotest", "sa", "", true);
        dataSource.setDriverClassName("org.hsqldb.jdbcDriver");
    }

    @AfterClass
    public static void clearDataSource() throws Exception {
        dataSource.destroy();
        dataSource = null;
    }*/
	
	
	CustomJdbcTokenRepositoryImpl(){
		super();
	}
	
	CustomJdbcTokenRepositoryImpl(boolean createTableOnStartup, DataSource dataSource){
		super();
		
		this.setCreateTableOnStartup(createTableOnStartup);
		this.setDataSource(dataSource);
		
	}
	
	public void createNewToken(PersistentRememberMeToken token){
		logger.debug("ENTER createNewToken");
		super.createNewToken(token);
		logger.debug("EXIT createNewToken");
	}
	
	public void removeUserTokens(String username){
		logger.debug("username: "+username);
		super.removeUserTokens(username);
	}
}
